<?php
include_once("config.php");
?>
<!DOCTYPE html>
<html lang="en-US" >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>CONTACT &#8211; Amin Associates Limited � Bangladesh</title>
        <meta name='robots' content='noindex,follow' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="assets/css/art.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='siteorigin-panels-front-css'  href='assets/css/front.css?ver=2.4.9' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-plugin-css-css'  href='assets/css/plugin.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-style-css'  href='assets/css/style.css' type='text/css' media='all' />
        <style>
            input[type=text], input[type=password],input[type=number],input[type=email] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

         

            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 20%;
                border-radius: 50%;
            }

            .containerlogin {
                padding: 16px;
                width: 100%;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modallogin {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modallogin-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 50%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
                to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
                to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
            .team-title {
                position: static;
                padding: 20px;
                display: inline-block;
                letter-spacing: 2px;
                width: 100%;
            }
        </style>
    </head>
    <body class="page page-id-1686 page-template-default siteorigin-panels">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- MAIN WRAPPER -->
        <div id="main-wrapper" class="animsition clearfix">
            <!-- start header -->
            <header id="masthead" class="site-header navbar-fixed-top">
                <div class="header-navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                        
                            <div class="logo navbar-brand">
                                <div class="logo-image">
                                    <a href="<?= $baseUrl ?>"><img src="image/logo-1.png" class="image-logo" alt="logo" /></a>
                                </div>
                            </div><!-- end logo -->
                            <nav id="primary-navigation" class="site-navigation navbar-collapse collapse" role="navigation">
                                <div class="nav-menu">
                                    <ul id="menu-all-pages" class="menu">
                                        <li><a href="<?= $baseUrl ?>">HOME</a></li>
                                        <li><a href="<?= $baseUrl ?>products.php">PRODUCTS</a></li>
                                        <li><a href="<?= $baseUrl ?>gallery.php">FACTORY GALLERY</a></li>
                                        <li><a href="<?= $baseUrl ?>about.php">ABOUT</a></li>
                                        <li class="active"><a href="<?= $baseUrl ?>contact.php">CONTACT</a></li>
                                        <li><a href="<?= $baseUrl ?>contact.php#appointment">Make an Appoinment</a></li>
                                    </ul>
                                </div><!-- end nav-menu -->
                            </nav><!-- end #primary-navigation -->
                            <div class="appoinment-header">
                                <a href="<?= $baseUrl ?>login/login.php"  target="_blank" class="btn btn-md btn-default" style="width:auto; background-color: #103b7a; color: #f9c937">Login</a>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end container-fluid -->
                </div><!-- end header-navigation -->
            </header>
            <!-- end of header -->
            <div id="content-wrapper" class="wrapper with-margin">
                <article  id="page-1684" class="page container post-1684 type-page status-publish hentry">
                    <!-- START CONTENT HERE -->
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                        <div class="so-widget-title-widget so-widget-title-widget-base">
                            <div class="the-title text-center dark">
                                <h3>Location</h3>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-8">
                                    <!-- start map -->
                                    <?php
                                    $result = $mysqli->query("SELECT * FROM address");
                                    $res = $result->fetch_object();
                                    $r1 = $result->num_rows;
                                    ?>
                                    <iframe src="https://www.google.com/maps/embed?pb=<?php echo $res->map ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    <!-- MAP END -->
                                </div>
                                <!-- end map -->
                                <!-- start location -->
                                <div class="col-md-4">
                                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                                        <div class="so-widget-title-widget so-widget-title-widget-base">
                                            <h4><p><strong>OFFICE ADDRESS</strong></p></h4>
                                            <strong><i class="fa fa-map-marker" aria-hidden="true"></i> Location</strong> : <br>
                                            <?php
                                            if
                                            ($r1 > 0) {
                                                echo $res->location;
                                            }
                                            ?>
                                            <br>
                                            <strong><i class="fa fa-phone" aria-hidden="true"></i> Phone </strong>: <br>
                                            <?php
                                            if
                                            ($r1 > 0) {
                                                echo $res->phone;
                                            }
                                            ?>
                                            <br>
                                            <strong><i class="fa fa-envelope" aria-hidden="true"></i> Email</strong>  : <br>                                            
                                            <?php
                                                   if
                                                   ($r1 > 0) {
                                                       echo $res->email;
                                                   }
                                                   ?></a><br> 
                                            <strong><i class="fa fa-clock-o" aria-hidden="true"></i> Open Hour</strong>  : <br>
                                            <?php
                                            if
                                            ($r1 > 0) {
                                                echo $res->openHour;
                                            }
                                            ?>
                                            <br>
                                        </div>
                                    </div><!-- end widget-content -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- start contact -->
                    <br>
                    <div id="appointment" class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                        <div class="so-widget-title-widget so-widget-title-widget-base">
                            <div class="the-title text-center dark">
                                <h3>CONTACT</h3>
                            </div>
                        </div>
                        <form action="msg.php" method="post" onSubmit="alert('Thank you for your contact.');">
                            <p class="team-title"><em>If you have any query about us, please make yourself comfortable and feel free to contact with us.</em></p>
                            <?php
                            if (isset($_GET['msg'])) {
                                ?>
                                <div class = "alert alert-success">
                                    <strong>SUCCESS!</strong> Your message has been send!!!
                                </div>
                                <?php
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><input type="text" name="msgName" size="40"  placeholder="Your name" required="required"/></p>
                                </div>
                                <div class="col-md-6">
                                    <p><input type="email" name="msgEmail" size="40" placeholder="Your email example@gmail.com" required="required"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><input type="text" name="msgPhn" minlength="11" maxlength="14" placeholder="Your phone number 01XXXXXXXXX" pattern="^(?:\+?88|0088)?01[15-9]\d{8}$" required="required"/></p>
                                    <span class="validity"></span>
                                </div>
                                <div class="col-md-6">
                                    <p><input type="text" name="msgSub" size="40" placeholder="Subject Here" required="required"/></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><textarea name="msgMsg" cols="20" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" style="resize: vertical;" placeholder="Type your message here.." required="required"></textarea></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><input type="submit" name="consubmit" value="Send Message" class="wpcf7-form-control wpcf7-submit btn btn-lg btn-outline" style="width:auto; background-color: #ffffff; color: #f9c937" /></p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end contact -->
                </article>
            </div>
        </div>
        <!-- END CONTENT HERE -->
        <!-- start footer-credit -->
        <div class="footer-credit">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copy">Amin Associates 2016 - Powered by <a href="http://www.olivineltd.com/"> OLIVINE LIMITED</a></p>
                    </div><!-- end column -->
                    <div class="col-md-6">
                        <ul class="list-socmed">
                            <li class="twitter soc-icon"><a href="http://twitter.com/#" class="fa fa-twitter"></a></li>
                            <li class="google soc-icon"><a href="http://google.com/#" class="fa fa-google-plus"></a></li>
                            <li class="facebook soc-icon"><a href="http://facebook.com/#" class="fa fa-facebook"></a></li>
                            <li class="linkedin soc-icon"><a href="http://linkedin.com/#" class="fa fa-linkedin"></a></li>
                            <li class="pinterest soc-icon"><a href="http://pinterest.com/#" class="fa fa-pinterest"></a></li>
                            <li class="youtube soc-icon"><a href="https://www.youtube.com/" class="fa fa-youtube"></a></li>
                            <li class="instagram soc-icon"><a href="https://instagram.com/" class="fa fa-instagram"></a></li>
                        </ul>
                    </div><!-- end column -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end footer-credit -->
        <!-- #main wrapper end-->
    </body>
</html>