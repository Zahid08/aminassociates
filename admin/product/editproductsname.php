<?php
ob_start();
include_once("../../config.php");
$id = $_GET['id'];
$result = $mysqli->query("SELECT * from productname where proid= '$id' ");
while ($res = $result->fetch_object()) {
    $proid = $res->proid;
    $name = $res->proname;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-edit"></i>&nbsp;UPDATE RODUCT TYPE</div>
                                    <div class="panel-body">
                                        <div class="row"> 
                                            <div class="col-md-8">
                                                <form action="../updateall.php" method="post">
                                                    <div class="row"> 
                                                        <div class="col-md-6">                                                             
                                                            <?php
                                                            if (isset($_GET['msg'])) {
                                                                ?>
                                                                <div class="alert alert-warning" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>FAILED! </strong> Product Type Name Must Be Different!!
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <div>
                                                                <input type="hidden" name="proid" value='<?php echo $proid; ?>' required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">  
                                                            <div>
                                                                <label for="products">PRODUCT TYPE NAME <b style="color:red;">*</b> </label><br>
                                                                <input type="text" name="proname"  value='<?php echo $name; ?>' style="height:40px; width: 100%" required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-md-3">  
                                                            <a href="productnamelist.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                        </div>
                                                        <div class="col-md-3">  
                                                            <button type="submit" name="pnameupdate" class="btn btn-default" style="width:100%; background-color: green; color: white">UPDATE TYPE</button><br><br><br>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive7").addClass("active");
            $("#dashActive7").parent().parent().addClass("treeview active");
            $("#dashActive7").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>