<?php

include_once("../../config.php");
$sms = "";
$msg = "";
$s = "";
if (isset($_POST["submit"])) {
    $pdiscrip = $_POST['pdiscrip'];
    $proname = $_POST['proname'];
    $pname = $_POST['pname'];
    $pimg = ($_FILES['pimg']['name']);
    $target_dir = __DIR__ . "/upload/product/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $x = $mysqli->query("SELECT * from product");
    $flag = 1;
    while ($res = $x->fetch_object()) {
        if (($proname === $res->ptype) && ($pname === $res->pname)) {
            $flag = 0;
        }
    }
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0 && $flag == 1) {
            move_uploaded_file($temp, __DIR__ . "/../upload/product/$pimg");
            $mysqli->query("INSERT INTO product(ptype,pname,pimg,pdiscrip) VALUES('$proname','$pname','$pimg','$pdiscrip')");
            header("Location: addproducts.php?sms");
        } if ($uploadOk == 0 && $flag == 0) {
            header("Location: addproducts.php?s");
        } if ($uploadOk == 2) {
            header("Location: addproducts.php?sms2");
        } if ($uploadOk == 3) {
            header("Location: addproducts.php?sms3");
        }
    } else {
        header("Location: addproducts.php?msg");
    }
}
?>