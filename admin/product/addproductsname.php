<?php
ob_start();
include_once("../../config.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-plus"></i>&nbsp;ADD PRODUCT TYPE</div>
                                    <div class="panel-body">
                                        <form action="addname.php" method="post">
                                            <div class="row"> 
                                                <div class="col-md-6">  
                                                    <?php
                                                    if (isset($_GET['sms'])) {
                                                        ?>
                                                        <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>Success!</strong> INSERT SUCCESSFUL!!!
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if (isset($_GET['msg'])) {
                                                        ?>
                                                        <div class="alert alert-warning" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>FAILED!</strong> TYPE NAME MUST BE DIFFERENT!!!
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <label for="products">NAME <b style="color:red;">*</b> </label><br>
                                                    <input type="text" name="proname"  style="height:40px; width: 100%"required/>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-3">  
                                                    <a href="productnamelist.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                </div>
                                                <div class="col-md-3">  
                                                    <button type="submit" name="submit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-plus fa-1x"></i>ADD</button><br><br><br>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive7").addClass("active");
            $("#tableActive7").parent().parent().addClass("treeview active");
            $("#tableActive7").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>