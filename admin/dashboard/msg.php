<?php
include_once("../../config.php");
$result = $mysqli->query("SELECT * FROM message ORDER BY msgId DESC");
if ($ru = $mysqli->query("SELECT msgId FROM message")) {

    /* determine number of rows result set */
    $r = $ru->num_rows;

    /* close result set */
    $result->close();
}
if (isset($_GET['count'])) {
//update table
    $mysqli->query("update message set stutus='Inactive' WHERE stutus ='Active'");
}
if (isset($_GET['id'])) {
    $id = $_GET['id'];

//deleting the row from table
//$result = mysql_query("DELETE FROM users WHERE id=$id");
    $mysqli->query("DELETE FROM message WHERE msgId=$id");
//redirecting to the display page (index.php in our case)
    header("Location:msg.php?dlt");
}
////////////////////////////////////////////////////////////////////////////////////
$perpage = 10;
if (isset($_GET['page']) & !empty($_GET['page'])) {
    $curpage = $_GET['page'];
} else {
    $curpage = 1;
}
$start = ($curpage * $perpage) - $perpage;
$PageSql = "SELECT * FROM message";
$pageres = $mysqli->query("SELECT * FROM message");
$totalres = mysqli_num_rows($pageres);

$endpage = ceil($totalres / $perpage);
$startpage = 1;
$nextpage = $curpage + 1;
$nextnextpage = $nextpage + 1;
$nextnextpage = $nextpage + 1;
$nextnextpage1 = $nextnextpage + 1;
$previouspage = $curpage - 1;

$ReadSql = "SELECT * FROM message LIMIT $start, $perpage";
$res = $mysqli->query("SELECT * FROM message order by msgId desc LIMIT $start, $perpage");
if ($result = $mysqli->query("SELECT msgId FROM message")) {

    /* determine number of rows result set */
    $row_cnt4 = $result->num_rows;

    /* close result set */
    $result->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Message</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
            .table , thead, tr,td{
                border: 1px solid #103b7a;
                border-collapse: collapse;
            }
            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: center;
            }
            tr:nth-child(odd) {
                background-color: #dcdcdc;
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: bottom;
            }
            .pagination> .active > a{
                background-color: #1E4770;
            }

        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style=" border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style="background-color: #1E4770; "><i class="fa fa-envelope"></i> &nbsp;MESSAGE DETAILS</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                if (isset($_GET['sms'])) {
                                                    ?>
                                                    <center>
                                                        <div class="alert alert-success" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <strong>SEND! </strong> Message Sending Successful!!
                                                        </div>
                                                    </center>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if (isset($_GET['dlt'])) {
                                                    ?>
                                                    <center>
                                                        <div class="alert alert-danger" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <strong>DELETE! </strong>Message Delete Successful!!
                                                        </div>
                                                    </center>
                                                    <?php
                                                }
                                                ?>
                                                <div style="overflow-x:auto;">
                                                    <table class="table" style="text-align: center;">
                                                        <tr>
                                                            <th>Sr#</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Phone No#</th>
                                                            <th>Subject</th>
                                                            <th>Message</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        <?php
                                                        $c = ($perpage * $curpage) - $perpage;
                                                        while ($test = $res->fetch_object()) {
                                                            $p1 = str_word_count($test->msgMsg);
                                                            $p2 = str_word_count($test->msgSub);
                                                            $id;
                                                            $c++;
                                                            $newtext1 = $test->msgMsg;
                                                            $newtext2 = $test->msgSub;
                                                            echo "<tr>";
                                                            echo "<td>" . $c . "</td>";
                                                            echo "<td>" . wordwrap($test->msgName, 20, "<br>\n") . "</td>";
                                                            echo "<td>" . wordwrap($test->msgEmail, 20, "<br>\n") . "</td>";
                                                            echo "<td>" . wordwrap($test->msgPhn, 14, "<br>\n") . "</td>";
                                                            echo "<td>" . substr($test->msgSub, 0, 15) . '. . .' . "</td>";
                                                            echo "<td>" . substr($newtext1, 0, 15) . '. . .'
                                                            ?>
                                                            <a data-toggle="modal" href="#myModal<?php echo $test->msgId; ?>" class="more"> View more</a>
                                                            <?php
                                                            echo "</td>";
                                                            echo "<td><a href=\"mail.php?id=$test->msgId\">Reply</a> | <a href=\"msg.php?id=$test->msgId\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";
                                                            ?>
                                                            <div class="modal fade" id="myModal<?php echo $test->msgId; ?>" role="dialog" style=" padding-top: 25px; background: rgba(0,0,0,0.01)">
                                                                <div class="modal-dialog">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header" style="border-bottom-color: #1E4770; background-color: whitesmoke">
                                                                            <button type="button" class="close" data-dismiss="modal" style="align:right;">&times;</button>
                                                                            <h3 class="modal-title">
                                                                                From: &nbsp;
                                                                                <?php
                                                                                echo $test->msgName;
                                                                                ?>
                                                                            </h3>
                                                                        </div>
                                                                        <div class="modal-body">                                                                            
                                                                            <b style=" font-size: 18px;">
                                                                                Sub: &nbsp;
                                                                            </b>
                                                                            <p>
                                                                                <?php
                                                                                echo $test->msgSub;
                                                                                ?>
                                                                            </p>
                                                                            <hr style="border-top-color: #1E4770;">
                                                                            <b style=" font-size: 18px;">Message:</b>
                                                                            <br>
                                                                            <p>
                                                                                <?php echo $test->msgMsg; ?>                                                                        
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer" style="border-top-color: #1E4770;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style=" background-color: #103b7a; color: #f9c937 ">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php }
                                                        ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php if ($r > 0) { ?>
                                                    <nav aria-label="Page navigation">
                                                        <ul class="pagination">
                                                            <?php if ($curpage != $startpage) { ?>
                                                                <li class="page-item">
                                                                    <a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $startpage ?>" tabindex="-1" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                        <span class="sr-only">First</span>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                            <?php if ($curpage >= 10) { ?>
                                                                <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $previouspage ?>"><?php echo $previouspage ?></a></li>
                                                            <?php } ?>
                                                            <li class="page-item active"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $curpage ?>"><?php echo $curpage ?></a></li>
                                                            <?php if ($curpage != $endpage) { ?>
                                                                <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $nextpage ?>"><?php echo $nextpage ?></a></li>
                                                                <?php if ($nextpage != $endpage) { ?>
                                                                    <li class="page-item"><a class="page-link"  style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $nextnextpage ?>"><?php echo $nextnextpage ?></a></li>
                                                                    <?php if ($nextnextpage != $endpage) { ?>
                                                                        <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px"  href="?page=<?php echo $nextnextpage1 ?>"><?php echo $nextnextpage1 ?></a></li>
                                                                        <li class="page-item">
                                                                            <a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $endpage ?>" aria-label="Next">
                                                                                <span aria-hidden="true">&raquo;</span>
                                                                                <span class="sr-only">Last</span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </nav>
                                                    <?php
                                                } else {
                                                    echo "<h4 style=\"color: red; text-align: center;\"><em>****No data available***</em></h4>";
                                                }
                                                ?>  
                                                <hr>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive2").addClass("active");
            $("#dashActive2").parent().parent().addClass("treeview active");
            $("#dashActive2").parent().addClass("in");
        </script>        
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>