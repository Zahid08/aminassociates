<?php
include_once("../../config.php");
$id = $_GET['id'];
$result = $mysqli->query("SELECT * from message where msgId= '$id' ");
while ($res = $result->fetch_object()) {
    $msgEmail = $res->msgEmail;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 80%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: bottom;
            }
            .pagination> .active > a{
                background-color: #1E4770;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style="background-color: #1E4770; "><i class="fa fa-reply"></i>&nbsp;REPLY MESSAGE </div>
                                    <div class="panel-body"><div class="row"> 
                                            <div class="col-md-8">
                                                <form action="../updateall.php" method="post" enctype="multipart/form-data">                                    
                                                    <table>
                                                        <?php
                                                        if (isset($_GET['sms'])) {
                                                            ?>
                                                            <center>
                                                                <div class="alert alert-success" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>SEND! </strong> Message Sending Successful!!
                                                                </div>
                                                            </center>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['dlt'])) {
                                                            ?>
                                                            <center>
                                                                <div class="alert alert-danger" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>DELETE! </strong>Message Delete Successful!!
                                                                </div>
                                                            </center>
                                                            <?php
                                                        }
                                                        ?>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="msgEmail">TO <b style="color:red;">*</b> </label>
                                                                    <input type="text" name="msgEmail" value='<?php echo $msgEmail; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="msgSub">SUBJECT <b style="color:red;">*</b> </label>
                                                                    <input type="text" name="msgSub" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="message"></i>MESSAGE <b style="color:red;">*</b> </label>
                                                                    <textarea name="msg" rows="5" cols="30"  style="width:100%"required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>     
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-3">  
                                                                <a href="msg.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                            </div>
                                                            <div class="col-md-3"> 
                                                                <button type="submit" name="msgupdate" class="btn btn-default" style="width:100%; background-color: green; color: white">REPLY</button>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive2").addClass("active");
            $("#dashActive2").parent().parent().addClass("treeview active");
            $("#dashActive2").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>