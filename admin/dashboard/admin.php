<?php
include_once("../../config.php");
if ($result = $mysqli->query("SELECT serviceId FROM service")) {

    /* determine number of rows result set */
    $row_cnt0 = $result->num_rows;

    /* close result set */
    $result->close();
}

if ($result = $mysqli->query("SELECT sliderId FROM slider")) {

    /* determine number of rows result set */
    $row_cnt1 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT memId FROM member")) {

    /* determine number of rows result set */
    $row_cnt2 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT pId FROM product")) {

    /* determine number of rows result set */
    $row_cnt3 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT msgId FROM message")) {

    /* determine number of rows result set */
    $row_cnt4 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT proid FROM productname")) {

    /* determine number of rows result set */
    $row_cnt5 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT facId FROM factory")) {

    /* determine number of rows result set */
    $row_cnt6 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT brandId FROM brand")) {

    /* determine number of rows result set */
    $row_cnt7 = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT contactId FROM contact")) {

    /* determine number of rows result set */
    $row_cnt8 = $result->num_rows;

    /* close result set */
    $result->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" hover: style="background-color:#1E4770;"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <!--BRAND-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color: bisque;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-bold fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>BRAND PARTNER</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt7</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/brand/brandlist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!--PERSON-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color: #a7d6d6;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-phone-square fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>CONTACT PERSON</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt8</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/contact/contactlist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!--IMAGE-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color:#9bb501b0;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-image fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>FACTORY IMAGE</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt6</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/factory/imagelist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!--MEMBERS-->
                                            <div class="col-md-3">
                                                <div class="panel panel-yellow" style="background-color:#ffa;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-users fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>MEMBERS</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt2</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/member/memberlist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">  
                                            <!--MESSAGE-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color: #e6c1c1;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-comments fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>MESSAGES</div>
                                                                <div class="huge"><?php
                                                                    echo "<b style=\"font-size:20px;\">$row_cnt4</b>";
                                                                    $count;
                                                                    ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="msg.php?count">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div> 
                                            <!--TYPE-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color: #ff9900b0;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-list fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>PRODUCT TYPES</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt5</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/product/productnamelist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div> 
                                            <!--products-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color:#b5c8cc;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-cart-plus fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>PRODUCTS</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt3</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/product/productlist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <!--SERVICE-->
                                            <div class="col-md-3">
                                                <div class="panel panel-green" style="background-color:#f95454c9;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-tasks fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>SERVICES</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt0</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/service/servicelist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <!--SLIDERS-->
                                            <div class="col-md-3">
                                                <div class="panel panel-red" style="background-color:#aaf;">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <i class="fa fa-slideshare fa-5x"></i>
                                                            </div>
                                                            <div class="col-xs-9 text-right">
                                                                <div>SLIDERS</div>
                                                                <div class="huge"><?php echo "<b style=\"font-size:20px;\">$row_cnt1</b>"; ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="<?= $baseUrl ?>admin/slider/sliderlist.php">
                                                        <div class="panel-footer" style="background-color:rgba(255,255,255,0.8);">
                                                            <span class="pull-left">View Details</span>
                                                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>                                                                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
    </div>
    <?php include '../layout/footer.php'; ?>
</div>
<?php include '../layout/footer_script.php'; ?>
<script type="text/javascript">
    $("#dashActive1").addClass("active");
    $("#dashActive1").parent().parent().addClass("treeview active");
    $("#dashActive1").parent().addClass("in");
</script>
</body>
</html>
