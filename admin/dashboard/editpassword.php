<?php
ob_start();
include_once("../../config.php");
$pw = $mysqli->query("SELECT * FROM admin");
$res = $pw->fetch_object();
$adminId = $res->adminId;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style="background-color:#1E4770;"><i class="fa fa-key"></i>&nbsp;CHANGE PASSWORD</div>
                                    <div class="panel-body">
                                        <div class="row"> 
                                            <div class="col-md-8">
                                                <form action="../updateall.php" method="post" enctype="multipart/form-data">
                                                    <table align="right">
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <?php
                                                                if (isset($_GET['msg'])) {
                                                                    ?>
                                                                    <div class="alert alert-warning" role="alert">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <strong>FAILED!</strong> Password not matched!!!
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <div>
                                                                    <input type="hidden" name="adminId" value='<?php echo $adminId; ?>'required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="adminPass"><i class="fa fa-key fa-1x"></i>OLD PASSWORD <b style="color:red;">*</b> </label><br>
                                                                    <input type="password" name="oldpass" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" placeholder="Type old password..." required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="adminpass"><i class="fa fa-key fa-1x"></i>NEW PASSWORD <b style="color:red;">*</b> </label><br>
                                                                    <input type="password" name="newpass"  placeholder="Type new password..." pattern="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/><label style="color: #0066cc">(**Password must contain minimum one letter and minimum length is six**)</label>
                                                                    <span class="validity"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="adminName"><i class="fa fa-key fa-1x"></i>CONFIRM NEW PASSWORD <b style="color:red;">*</b> </label><br>
                                                                    <input type="password" name="cnfirmpass"  placeholder="Retype new password..." pattern="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                                    <span class="validity"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <br>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-3">  
                                                                <a href="profile.php"><button class="btn btn-primary" type="button" style="width:100%; background-color:#1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO PROFILE</button></a>&nbsp;
                                                            </div>
                                                            <div class="col-md-3">  
                                                                <button type="submit" name="passupdate" class="btn btn-default" style="width:100%; background-color: green; color: white"><strong>UPDATE PASSWORD</strong></button><br><br><br>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive3").addClass("active");
            $("#dashActive3").parent().parent().addClass("treeview active");
            $("#dashActive3").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>
