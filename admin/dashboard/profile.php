<?php
include_once("../../config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 75%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style="background-color:#1E4770;"><i class="fa fa-user"></i>&nbsp;PROFILE</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <center>
                                                    <div class="panel panel-default" style="width: 70%; background-color: #fffffc">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <?php
                                                                    if (isset($_GET['msg'])) {
                                                                        ?>
                                                                        <div class="alert alert-success" role="alert">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <strong>SUCCESS!</strong> Data Update successful!!!
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    if (isset($_GET['smsp'])) {
                                                                        ?>
                                                                        <div class="alert alert-success" role="alert">
                                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <strong>SUCCESS!</strong> Password Update successful!!!
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                    $result = $mysqli->query("SELECT * FROM admin");
                                                                    while ($res = $result->fetch_object()) {
                                                                        echo "<center>";
                                                                        echo "<img src =\"../upload/$res->adminPic\" width = \"100px\" height = \"100px\" class=\"img-circle\" class=\"img-thumbnail\">";
                                                                        echo "<br>";
                                                                        echo "</center>";
                                                                        echo "<h1>" . $res->adminName . "</h1>";
                                                                        echo '<hr>';
                                                                        ?>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6" style="float: left">
                                                                            <?php
                                                                            echo "<strong style=\"font-size: 18px\">" . "Phone:&nbsp;" . "</strong>";
                                                                            //echo '<hr>';
                                                                            ?>
                                                                        </div>
                                                                        <div class="col-md-6" style="float: right">
                                                                            <?php
                                                                            echo "<p style=\"font-size: 18px\">" . $res->adminPhn . "</p>";
                                                                            //echo '<hr>';
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                        <div class="col-md-6" style="float: left">
                                                                            <?php
                                                                            echo "<strong style=\"font-size: 18px\">" . "Email:&nbsp;" . "</strong>";
                                                                            //echo '<hr>';
                                                                            ?>
                                                                        </div>
                                                                        <div class="col-md-6" style="float: right">
                                                                            <?php
                                                                            echo "<p style=\"font-size: 18px\">" . $res->adminEmail . "</p>";
                                                                            //echo '<hr>';
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <a href="editprofile.php?"><button style="background-color:#006600; width: 35%; height: 50px; color: white"><i class="fa fa-edit fa-1x"></i><strong style="font-size: 16px"> Update Profile</strong></button></a>
                                                                    &nbsp;
                                                                    <a href="editpassword.php?"><button style="background-color: #006600; width: 35%;  height: 50px; color: white"><i class="fa fa-key fa-1x"></i><strong style="font-size: 16px"> Change Password</strong></button></a>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive3").addClass("active");
            $("#dashActive3").parent().parent().addClass("treeview active");
            $("#dashActive3").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>