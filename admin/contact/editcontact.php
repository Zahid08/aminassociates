<?php
ob_start();
include_once("../../config.php");
$id = $_GET['id'];
$result = $mysqli->query("SELECT * from contact where contactId= '$id' ");
while ($res = $result->fetch_object()) {
    $contactId = $res->contactId;
    $contactName = $res->contactName;
    $contactRole = $res->contactRole;
    $contactPhone = $res->contactPhone;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Settings</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;;"><i class="fa fa-phone"></i> &nbsp;UPDATE CONTACT</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <form action="../updateall.php" method="post">
                                                    <table>
                                                        <tr>
                                                            <?php
                                                            if (isset($_GET['sms'])) {
                                                                ?>
                                                            <div class="alert alert-success" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SUCCESS!</strong> Data insert successful!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['msg'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>FAILED!</strong> There is no file!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>    
                                                        <div>
                                                            <input type="hidden" name="contactId" value='<?php echo $contactId; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                        </div>
                                                        <div>
                                                            <label for = "Name"><i class = "fa fa-user"></i>PERSON NAME <b style="color:red;">*</b> </label>
                                                            <input type = "text" name = "contactName" value='<?php echo $contactName; ?>' style = "width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"required/>
                                                        </div>          
                                                        <tr> 
                                                        <div>
                                                            <label for="role">PERSON ROLE <b style="color:red;">*</b> </label>
                                                            <input type="text" name="contactRole" value='<?php echo $contactRole; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div>
                                                            <label for="phone"><i class="fa fa-phone"></i>PERSON PHONE <b style="color:red;">*</b> </label>
                                                            <textarea name="contactPhone" rows="5" cols="30" style="width:100%; padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box; resize: vertical;"required><?php echo $contactPhone; ?></textarea>
                                                        </div>
                                                        </tr>     
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <a href="contactlist.php"><button class="btn btn-primary" type="button" style="width:100%;background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                            </div>
                                                            <div class="col-md-6">  
                                                                <button type="submit" name="contactupdate" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-edit"></i> UPDATE CONTACT</button><br><br><br>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </section>
                        </div>
                        <?php include '../layout/footer.php'; ?>
                    </div>
                    <?php include '../layout/footer_script.php'; ?>
                    <script type="text/javascript">
                        $("#dashActive11").addClass("active");
                        $("#dashActive11").parent().parent().addClass("treeview active");
                        $("#dashActive11").parent().addClass("in");
                    </script>
                    </body>
                    </html>
                    <script>
                        window.setTimeout(function () {
                            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                                $(this).remove();
                            });
                        }, 4000);
                    </script>
