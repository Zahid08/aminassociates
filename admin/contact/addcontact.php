<?php
ob_start();
include_once("../../config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | Add Brand</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style=" border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-plus-circle"></i>&nbsp;ADD CONTACT</div>

                                    <div class="panel-body">
                                        <form action="contact.php" method="post">
                                            <BR>
                                            <table style="align:center">
                                                <tr>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div>
                                                            <?php
                                                            if (isset($_GET['sms'])) {
                                                                ?>
                                                                <div class="alert alert-success" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>SUCCESS!</strong> Data insert successful!!!
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <label for="name">PERSON NAME <b style="color:red;">*</b> </label><br>
                                                            <input type="text" name="contactName" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                        </div>
                                                        <div>

                                                            <label for="role"> PERSON ROLE <b style="color:red;">*</b> </label><br>
                                                            <input type="text" name="contactRole" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                        </div>
                                                        <div>

                                                            <label for="phone"><i class="fa fa-phone fa-1x"></i> &nbsp;PERSON PHONE <b style="color:red;">*</b> </label><br>
                                                            <input type="text" name="contactPhone" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                                </tr>
                                                <tr>
                                                <div class="row">
                                                    <div class="col-md-3">  
                                                        <a href="contactlist.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i>&nbsp; BACK TO LIST</button></a>&nbsp;
                                                    </div>
                                                    <div class="col-md-3">  
                                                        <button type="submit" name="submit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-plus fa-1x"></i> &nbsp;ADD CONTACT</button><br><br><br>
                                                    </div>
                                                </div>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>

    </body>
</html>
<script type="text/javascript">
    $("#tableActive20").addClass("active");
    $("#tableActive20").parent().parent().addClass("treeview active");
    $("#tableActive20").parent().addClass("in");
</script>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>