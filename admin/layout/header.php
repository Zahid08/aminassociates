<?php
session_start();
if (!$_SESSION["adminName"]) {
    header("Location: $baseUrl");
}
?>
<?php
include_once("../../config.php");
$count;
if ($result = $mysqli->query("SELECT msgId FROM message where stutus='Active'")) {

    /* determine number of rows result set */
    $row_cnt = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT msgId FROM message")) {

    /* determine number of rows result set */
    $row_cnt4 = $result->num_rows;

    /* close result set */
    $result->close();
}
?>
<header class="main-header">
    <a href="<?= $baseUrl ?>admin/dashboard/admin.php" class="logo">
        <span class="logo-mini" style="font-size: 15px;"><b>ADMIN</b></span>
        <span class="logo-lg" style="font-size: 15px;"><b>System Admin Panel</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation" style="background-color: #441f5d">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell fa-1x"></i>&nbsp;Message
                        <?php if ($row_cnt > 0) { ?>
                            <span class="label label-primary" style=" font-size: 13px;"><?php printf("%d\n", $row_cnt); ?></span>
                        <?php } ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?php printf("%d\n", $row_cnt4); ?> messages</li>                        
                        <li class="footer"><a href="<?= $baseUrl ?>admin/dashboard/msg.php?count">See All Messages</a></li>
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span><i class="fa fa-user"></i>&nbsp;
                            <?php
                            $result = $mysqli->query("SELECT * FROM admin");
                            while ($test = $result->fetch_object()) {
                                echo "Welcome! &nbsp;" . $test->adminName . "&nbsp;&nbsp;";
                            }
                            ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu" >                        
                        <li class="user-footer" style="background-color: white;">
                            <div class="pull-left">
                                <a href="<?= $baseUrl ?>admin/dashboard/profile.php" class="btn btn-info btn-flat" style="border: 1px solid #3C8DBC;">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="../layout/logout.php" class="btn btn-danger btn-flat" style="border: 1px solid #3C8DBC;">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>