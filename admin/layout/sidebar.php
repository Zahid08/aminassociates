<aside class="main-sidebar" style="background-color: #441f5d; color: #13013e">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header" style="background-color: #441f5d">
                <p style="font-size: 30px; text-align: center">
                    <?php
                    $result = $mysqli->query("SELECT * FROM admin");
                    while ($test = $result->fetch_object()) {
                        ?>
                    <a href="<?= $baseUrl ?>admin/dashboard/profile.php" ><img src = "<?= $baseUrl ?>admin/upload/<?php echo $test->adminPic; ?>" class="img-circle" width="100" height="100"></a><br>
                        <?php
                    }

                    $result = $mysqli->query("SELECT * FROM admin");
                    while ($test = $result->fetch_object()) {
                        echo $test->adminName;
                    }
                    ?>
                </p>
            </li>
            <li class="treeview" id="dashActive1">
                <a href="<?= $baseUrl ?>admin/dashboard/admin.php">
                    <i class="fa fa-dashboard"></i><span>DASHBOARD</span>
                </a>
            </li>        
            <li class="treeview" id="dashActive2">
                <?php $count; ?>
                <a href="<?= $baseUrl ?>admin/dashboard/msg.php?count">
                    <i class="fa fa-bell fa-fw"></i><span>MESSAGE</span>
                </a>
            </li>
            <li class="treeview" id="dashActive3">
                <a href="<?= $baseUrl ?>admin/dashboard/profile.php"><i class="fa fa-user fa-fw"></i>PROFILE</a>
            </li>  
            <li class="treeview" id="dashActive4">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>BRAND PARTNER</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive1"><a href="<?= $baseUrl ?>admin/brand/addbrand.php"><i class="fa fa-plus-circle"></i>ADD BRAND</a></li>
                    <li id="tableActive2"><a href="<?= $baseUrl ?>admin/brand/brandlist.php"><i class="fa fa-list-ul"></i> BRAND LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive11">
                <a href="#">
                    <i class="fa fa-phone"></i>
                    <span>CONTACT PERSON</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive20"><a href="<?= $baseUrl ?>admin/contact/addcontact.php"><i class="fa fa-plus-circle"></i>ADD CONTACT</a></li>
                    <li id="tableActive21"><a href="<?= $baseUrl ?>admin/contact/contactlist.php"><i class="fa fa-list-ul"></i> CONTACT LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive5">
                <a href="#">
                    <i class="fa fa-image"></i>
                    <span>FACTORY GALLERY</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive3"><a href="<?= $baseUrl ?>admin/factory/addimage.php"><i class="fa fa-plus-circle"></i>ADD IMAGE</a></li>
                    <li id="tableActive4"><a href="<?= $baseUrl ?>admin/factory/imagelist.php"><i class="fa fa-list-ul"></i> IMAGE LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive6">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>MEMBER</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive5"><a href="<?= $baseUrl ?>admin/member/addmember.php"><i class="fa fa-plus-circle"></i> ADD MEMBER</a></li>
                    <li id="tableActive6"><a href="<?= $baseUrl ?>admin/member/memberlist.php"><i class="fa fa-list-ul"></i></i> MEMBER LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive7">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>PRODUCT</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive7"><a href="<?= $baseUrl ?>admin/product/addproductsname.php"><i class="fa fa-plus-circle"></i> ADD PRODUCT TYPE</a></li>
                    <li id="tableActive8"><a href="<?= $baseUrl ?>admin/product/productnamelist.php"><i class="fa fa-list-ul"></i> PRODUCT TYPE LIST</a></li>
                    <li id="tableActive9"><a href="<?= $baseUrl ?>admin/product/addproducts.php"><i class="fa fa-plus-circle"></i> ADD PRODUCT</a></li>
                    <li id="tableActive10"><a href="<?= $baseUrl ?>admin/product/productlist.php"><i class="fa fa-list-ul"></i> PRODUCT LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive8">
                <a href="#">
                    <i class="fa fa-tasks"></i>
                    <span>SERVICE</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive11"><a href="<?= $baseUrl ?>admin/service/addservice.php"><i class="fa fa-plus-circle"></i> ADD SERVICE</a></li>
                    <li id="tableActive12"><a href="<?= $baseUrl ?>admin/service/servicelist.php"><i class="fa fa-list-ul"></i></i> SERVICE LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive9">
                <a href="#">
                    <i class="fa fa-slideshare"></i>
                    <span>SLIDER</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive13"><a href="<?= $baseUrl ?>admin/slider/addslider.php"><i class="fa fa-plus-circle"></i> ADD SLIDER IMAGE</a></li>
                    <li id="tableActive14"><a href="<?= $baseUrl ?>admin/slider/sliderlist.php"><i class="fa fa-list-ul"></i></i> SLIDER IMAGE LIST</a></li>
                </ul>
            </li>
            <li class="treeview" id="dashActive10">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>SETTINGS</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li id="tableActive15"><a href="<?= $baseUrl ?>admin/settings/about.php"><i class="fa fa-edit"></i>CHANGE ABOUT</a></li>
                    <li id="tableActive16"><a href="<?= $baseUrl ?>admin/settings/picture.php"><i class="fa fa-edit"></i> CHANGE ABOUT PICTURE</a></li>
                    <li id="tableActive17"><a href="<?= $baseUrl ?>admin/settings/address.php"><i class="fa fa-edit"></i>CHANGE OFFFICE <br>INFORMATION</a></li>
                    <li id="tableActive19"><a href="<?= $baseUrl ?>admin/settings/location.php"><i class="fa fa-edit"></i>CHANGE MAP</a></li>

                </ul>
            </li>
            <li class="treeview" id="dashActive11">
                <a href="../layout/logout.php">
                    <i class="fa fa-sign-out"></i>
                    <span>SIGN OUT</span>
                </a>
            </li>  
        </ul>
    </section>
</aside>