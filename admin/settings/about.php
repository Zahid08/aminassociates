<?php
ob_start();
include_once("../../config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Settings</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: left;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-cogs"></i>&nbsp;CHANGE ABOUT</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <form action="setting.php" method="post">
                                                    <table>
                                                        <tr>
                                                            <th>ABOUT &nbsp;<b style="color:red;">*</b> </th>
                                                        </tr>
                                                        <td>
                                                            <?php
                                                            if (isset($_GET['sms1'])) {
                                                                ?>
                                                                <div class="alert alert-success" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>SUCCESS!</strong> Data update successful!!!
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <?php
                                                            if (isset($_GET['msg'])) {
                                                                ?>
                                                                <div class="alert alert-warning" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <strong>FAILED!</strong> There is no data to update!!!
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <div>
                                                                <?php
                                                                $result = $mysqli->query("SELECT * FROM about");
                                                                $res = $result->fetch_object();
                                                                if ($result = $mysqli->query("SELECT aboutId FROM about")) {

                                                                    /* determine number of rows result set */
                                                                    $r = $result->num_rows;

                                                                    /* close result set */
                                                                    $result->close();
                                                                }
                                                                ?>
                                                                <textarea name = "about" rows = "15" style="resize: vertical; width: 100%;"><?php
                                                                    if
                                                                    ($r > 0) {
                                                                        echo $res->about;
                                                                    }
                                                                    ?></textarea>
                                                            </div>  
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-3">  
                                                                </div>
                                                                <div class="col-md-6 col-sm-3">  
                                                                    <button type="submit" name="asubmit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-edit"></i> UPDATE ABOUT</button><br><br><br>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive15").addClass("active");
            $("#tableActive15").parent().parent().addClass("treeview active");
            $("#tableActive15").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>