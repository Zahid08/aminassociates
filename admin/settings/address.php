<?php
ob_start();
include_once("../../config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Settings</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-cogs"></i>&nbsp;CHANGE OFFICE INFORMATION</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <form action="setting.php" method="post">
                                                    <table>
                                                        <tr>
                                                            <?php
                                                            if (isset($_GET['sms'])) {
                                                                ?>
                                                            <div class="alert alert-success" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SUCCESS!</strong> Data insert successful!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['msg'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>FAILED!</strong> There is no file!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>    
                                                        <?php
                                                        $result = $mysqli->query("SELECT * FROM address where addressId=1");
                                                        $res = $result->fetch_object();
                                                        $r1 = $result->num_rows;
                                                        ?>
                                                        <div>
                                                            <label for = "address"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; ADDRESS <b style="color:red;">*</b> </label>
                                                            <textarea name="address" rows="5" cols="30" style="width:100%; resize: vertical" required="required"><?php
                                                                if
                                                                ($r1 > 0) {
                                                                    echo $res->location;
                                                                }
                                                                ?></textarea>
                                                        </div>            
                                                        <tr> 
                                                        <div>
                                                            <label for="phone"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp; PHONE <b style="color:red;">*</b> </label>
                                                            <textarea name="phone" rows="5" cols="30" style="width:100%; resize: vertical" required="required"><?php
                                                                if
                                                                ($r1 > 0) {
                                                                    echo $res->phone;
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div>
                                                            <label for="email"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; EMAILS <b style="color:red;">*</b> </label>
                                                            <textarea name="email" rows="5" cols="30" style="width:100%; resize: vertical" required="required"><?php
                                                                if
                                                                ($r1 > 0) {
                                                                    echo $res->email;
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        </tr>     
                                                        <tr>
                                                        <div>
                                                            <label for="hour"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp; OPEN HOUR <b style="color:red;">*</b> </label>
                                                            <textarea name="hour" rows="5" cols="30" style="width:100%; resize: vertical" required="required"><?php
                                                                if
                                                                ($r1 > 0) {
                                                                    echo $res->openHour;
                                                                }
                                                                ?></textarea>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                            </div>
                                                            <div class="col-md-6">  
                                                                <button type="submit" name="oisubmit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-edit fa-1x"></i> UPDATE INFORMATION</button><br><br><br>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive17").addClass("active");
            $("#tableActive17").parent().parent().addClass("treeview active");
            $("#tableActive17").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>