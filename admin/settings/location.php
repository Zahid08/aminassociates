<?php
ob_start();
include_once("../../config.php");
$result = $mysqli->query("SELECT * from address where addressId= 1 ");
$res = $result->fetch_object();
$r = $result->num_rows;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Settings</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-cogs"></i>&nbsp;CHANGE MAP</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <form action="setting.php" method="post">
                                                    <table>
                                                        <tr>
                                                            <?php
                                                            if (isset($_GET['sms3'])) {
                                                                ?>
                                                            <div class="alert alert-success" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SUCCESS!</strong> Data insert successful!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['msg3'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>FAILED!</strong> There is no data to update!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <div>
                                                            <label><i class="fa fa-map-marker"></i>&nbsp; LOCATION</label>
                                                            <textarea  name = "map" rows="10" style = "width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box; resize: vertical;"><?php
                                                                if
                                                                ($r > 0) {
                                                                    echo $res->map;
                                                                }
                                                                ?></textarea>
                                                        </div>         
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                            </div>
                                                            <div class="col-md-6">  
                                                                <button type="submit" name="lsubmit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-edit"></i> UPDATE MAP</button><br><br><br>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <?php
                                            $result = $mysqli->query("SELECT * FROM address");
                                            $res = $result->fetch_object();
                                            $r = $result->num_rows;
                                            if ($r > 0 && $res->map != NULL) {
                                                ?>
                                                <div class="col-md-6">
                                                    <table>                                                    
                                                        <tr>
                                                            <th>MAP</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <iframe src="https://www.google.com/maps/embed?pb=<?php echo $res->map ?>" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                            </td>
                                                        </tr>                                                    
                                                    </table>
                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive19").addClass("active");
            $("#tableActive19").parent().parent().addClass("treeview active");
            $("#tableActive19").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>