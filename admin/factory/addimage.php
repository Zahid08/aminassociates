<?php
ob_start();
include_once("../../config.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
            .image-preview-input {
                position: relative;
                overflow: hidden;
                margin: 0px;    
                color: #333;
                background-color: #fff;
                border-color: #ccc;    
            }
            .image-preview-input input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                padding: 0;
                font-size: 20px;
                cursor: pointer;
                opacity: 0;
                filter: alpha(opacity=0);
            }
            .image-preview-input-title {
                margin-left:2px;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-image"></i>&nbsp;ADD FACTORY IMAGE </div>
                                    <div class="panel-body">
                                        <form action="image.php" method="post" enctype="multipart/form-data">
                                            <BR>
                                            <table style="align:center">
                                                <tr>
                                                <div class="row"> 
                                                    <div class="col-md-6"> 
                                                        <?php
                                                        if (isset($_GET['sms'])) {
                                                            ?>
                                                            <div class="alert alert-success" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SUCCESS!</strong> Data insert successful!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['msg'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>FAILED!</strong> There is no file!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['sms2'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SORRY!</strong> your file is too large.Size must be less than 2mb!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?php
                                                        if (isset($_GET['sms3'])) {
                                                            ?>
                                                            <div class="alert alert-warning" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <strong>SORRY!</strong> only JPG, JPEG, PNG & GIF files are allowed.!!!
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        <div>
                                                            <label for="serviceName">IMAGE NAME  </label><br>
                                                            <input type="text" name="facName" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" />
                                                        </div>
                                                    </div>
                                                </div>
                                                </tr>
                                                <tr>
                                                <div class="row"> 
                                                    <div class="col-md-6">  
                                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                                        <div>
                                                            <label for="picture"><i class="fa fa-camera"></i>&nbsp;PICTURE <b style="color:red;">*</b> </label>
                                                            <div class="input-group image-preview">
                                                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                                <span class="input-group-btn">
                                                                    <!-- image-preview-clear button -->
                                                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                                                    </button>
                                                                    <!-- image-preview-input -->
                                                                    <div class="btn btn-default image-preview-input">
                                                                        <span class="glyphicon glyphicon-folder-open"></span>
                                                                        <span class="image-preview-input-title">Browse</span>
                                                                        <input type="file" accept="image/png, image/jpg, image/jpeg, image/gif" name="pimg" id="img" required/> <!-- rename it -->
                                                                    </div>
                                                                </span>
                                                            </div><!-- /input-group image-preview [TO HERE]--> 
                                                        </div>
                                                        <?php
                                                        if (isset($_GET['error'])) {
                                                            echo "<h4 style='color:red'><strong>*Invalid Picture Format</strong><h4>";
                                                        }
                                                        ?>
                                                        <script>
                                                            $(document).on('click', '#close-preview', function () {
                                                                $('.image-preview').popover('hide');
                                                                // Hover befor close the preview
                                                                $('.image-preview').hover(
                                                                        function () {
                                                                            $('.image-preview').popover('show');
                                                                        },
                                                                        function () {
                                                                            $('.image-preview').popover('hide');
                                                                        }
                                                                );
                                                            });

                                                            $(function () {
                                                                // Create the close button
                                                                var closebtn = $('<button/>', {
                                                                    type: "button",
                                                                    text: 'x',
                                                                    id: 'close-preview',
                                                                    style: 'font-size: initial;',
                                                                });
                                                                closebtn.attr("class", "close pull-right");
                                                                // Set the popover default content
                                                                $('.image-preview').popover({
                                                                    trigger: 'manual',
                                                                    html: true,
                                                                    title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
                                                                    content: "There's no image",
                                                                    placement: 'right'
                                                                });
                                                                // Clear event
                                                                $('.image-preview-clear').click(function () {
                                                                    $('.image-preview').attr("data-content", "").popover('hide');
                                                                    $('.image-preview-filename').val("");
                                                                    $('.image-preview-clear').hide();
                                                                    $('.image-preview-input input:file').val("");
                                                                    $(".image-preview-input-title").text("Browse");
                                                                });
                                                                // Create the preview image
                                                                $(".image-preview-input input:file").change(function () {
                                                                    var img = $('<img/>', {
                                                                        id: 'dynamic',
                                                                        width: 250,
                                                                        height: 200
                                                                    });
                                                                    var file = this.files[0];
                                                                    var reader = new FileReader();
                                                                    // Set preview image into the popover data-content
                                                                    reader.onload = function (e) {
                                                                        $(".image-preview-input-title").text("Change");
                                                                        $(".image-preview-clear").show();
                                                                        $(".image-preview-filename").val(file.name);
                                                                        img.attr('src', e.target.result);
                                                                        $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
                                                                    }
                                                                    reader.readAsDataURL(file);
                                                                });
                                                            });

                                                        </script>
                                                    </div>
                                                </div>  
                                                </tr>
                                                <tr>
                                                <div class="row">
                                                    <div class="col-md-6">  
                                                        <div>
                                                            <label for="discription"><i class="fa fa-info-circle"></i>&nbsp;DISCRIPTION  </label><br>
                                                            <textarea name="facDis" rows="5" cols="70" style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                </tr>
                                                <tr>
                                                <div class="row">
                                                    <div class="col-md-3">  
                                                        <a href="imagelist.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                    </div>
                                                    <div class="col-md-3">  
                                                        <button type="submit" name="submit" class="btn btn-default" style="width:100%; background-color:green; color: white"><i class="fa fa-plus fa-1x"></i>ADD</button><br><br><br>
                                                    </div>
                                                </div>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive3").addClass("active");
            $("#tableActive3").parent().parent().addClass("treeview active");
            $("#tableActive3").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>