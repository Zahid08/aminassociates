<?php

include_once("../../config.php");
$sms = $sms1 = $sms2 = $sms3 = "";
$msg = "";
$service = $mysqli->query("SELECT serviceName from service");
if (isset($_POST["submit"])) {
    $serviceName = $_POST['serviceName'];
    $serviceDis = $_POST['serviceDis'];
    $pimg = ($_FILES['pimg']['name']);
    $target_dir = __DIR__ . "/upload/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $flag = 0;
    while ($res = $service->fetch_object()) {
        if (($serviceName === $res->serviceName)) {
            $flag = 1;
        }
    }
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0 && $flag == 0) {
            move_uploaded_file($temp, __DIR__ . "/../upload/service/$pimg");
            $mysqli->query("INSERT INTO service(serviceName,pimg,serviceDis) VALUES('$serviceName','$pimg','$serviceDis')");
            header("Location: addservice.php?sms");
        }
        if ($uploadOk == 0 && $flag == 1) {
            header("Location: addservice.php?sms1");
        }
        if ($uploadOk == 2) {
            header("Location: addservice.php?sms2");
        }
        if ($uploadOk == 3) {
            header("Location: addservice.php?sms3");
        }
    } else {
        header("Location: addservice.php?msg");
    }
}
?>