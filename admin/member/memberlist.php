<?php
ob_start();
include_once("../../config.php");
$result = $mysqli->query("SELECT * FROM member");
if (isset($_GET['id'])) {
    $id = $_GET['id'];

//deleting the row from table
//$result = mysql_query("DELETE FROM users WHERE id=$id");
    $mysqli->query("DELETE FROM member WHERE memId=$id");
//redirecting to the display page (index.php in our case)
    header("Location:memberlist.php?dlt");
}
////////////////////////////////////////////////////////////////////////////////////
$perpage = 10;
if (isset($_GET['page']) & !empty($_GET['page'])) {
    $curpage = $_GET['page'];
} else {
    $curpage = 1;
}
$start = ($curpage * $perpage) - $perpage;
$PageSql = "SELECT * FROM member";
$pageres = $mysqli->query("SELECT * FROM member");
$totalres = mysqli_num_rows($pageres);

$endpage = ceil($totalres / $perpage);
$startpage = 1;
$nextpage = $curpage + 1;
$nextnextpage = $nextpage + 1;
$nextnextpage1 = $nextnextpage + 1;
$previouspage = $curpage - 1;

$ReadSql = "SELECT * FROM member LIMIT $start, $perpage";
$res = $mysqli->query("SELECT * FROM member ORDER BY memId desc LIMIT $start, $perpage");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include '../layout/header_script.php'; ?>
        <style>            
            .table , thead, tr,td{
                border: 1px solid #103b7a;
                border-collapse: collapse;
            }
            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: center;
            }
            tr:nth-child(odd) {
                background-color:#dcdcdc;
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: bottom;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }         
            .pagination> .active > a{
                background-color: #1E4770;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style=" border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-users"></i>&nbsp;MEMBER DETAILS</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                if (isset($_GET['dlt'])) {
                                                    ?>
                                                    <div class="alert alert-danger" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>DELETE! </strong> Data Delete successful!!
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <?php
                                                if (isset($_GET['msg'])) {
                                                    ?>
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong>SUCCESS!</strong> Data update successful!!!
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div style="overflow-x:auto;">
                                                    <table class="table" style="text-align: center;">
                                                        <tr>
                                                            <th>ID#</th>
                                                            <th>Name</th>
                                                            <th>Image</th>
                                                            <th>Designation</th>
                                                            <th>Discription</th>
                                                            <th>Google</th>                                        
                                                            <th>Twitter</th>
                                                            <th>Facebook</th>
                                                            <th>Action</th>
                                                        </tr>
                                                        <?php
                                                        $c = ($perpage * $curpage) - $perpage;
                                                        while ($rest = $res->fetch_object()) {
                                                            $c++;
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $c; ?></td>
                                                                <td><?php echo wordwrap($rest->memName, 15, "<br>\n"); ?></td>
                                                                <td><img src = "<?= $baseUrl ?>admin/upload/<?php echo $rest->pimg ?>" width = "100px" height = "100px"></td>
                                                                <td><?php echo wordwrap($rest->memRole, 20, "<br>\n"); ?></td>
                                                                <td><?php echo wordwrap($rest->memDis, 30, "<br>\n"); ?></td>
                                                                <td><?php echo wordwrap($rest->memGoogle, 20, "<br>\n"); ?></td>
                                                                <td><?php echo wordwrap($rest->memTwiter, 20, "<br>\n"); ?></td>
                                                                <td><?php echo wordwrap($rest->memFacebook, 20, "<br>\n"); ?></td>
                                                                <td><?php echo "<a href=\"editmember.php?id=$rest->memId\">Update</a> | <a href=\"memberlist.php?id=$rest->memId\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a>" ?></td>
                                                            </tr>
                                                        <?php } ?>                                    
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <?PHP
                                            if ($bu = $mysqli->query("SELECT memId FROM member")) {

                                                /* determine number of rows result set */
                                                $b = $bu->num_rows;

                                                /* close result set */
                                                $result->close();
                                            }
                                            if ($b > 0) {
                                                ?>
                                                <div class="col-md-6">
                                                    <nav aria-label="Page navigation">
                                                        <ul class="pagination">
                                                            <?php if ($curpage != $startpage) { ?>
                                                                <li class="page-item">
                                                                    <a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $startpage ?>" tabindex="-1" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                        <span class="sr-only">First</span>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                            <?php if ($curpage >= 10) { ?>
                                                                <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $previouspage ?>"><?php echo $previouspage ?></a></li>
                                                            <?php } ?>
                                                            <li class="page-item active"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $curpage ?>"><?php echo $curpage ?></a></li>
                                                            <?php if ($curpage != $endpage) { ?>
                                                                <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $nextpage ?>"><?php echo $nextpage ?></a></li>
                                                                <?php if ($nextpage != $endpage) { ?>
                                                                    <li class="page-item"><a class="page-link"  style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $nextnextpage ?>"><?php echo $nextnextpage ?></a></li>
                                                                    <?php if ($nextnextpage != $endpage) { ?>
                                                                        <li class="page-item"><a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px"  href="?page=<?php echo $nextnextpage1 ?>"><?php echo $nextnextpage1 ?></a></li>
                                                                        <li class="page-item">
                                                                            <a class="page-link" style=" width: 80px;line-height: 2.42857143; font-size: 14px; padding-left: 30px" href="?page=<?php echo $endpage ?>" aria-label="Next">
                                                                                <span aria-hidden="true">&raquo;</span>
                                                                                <span class="sr-only">Last</span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </nav>
                                                </div>
                                                <div class="col-md-6" style=" padding: 25px">
                                                    <a href="addmember.php"><button class="btn btn-primary" type="button" style=" width: 150px;background-color: green; float: right; height: 45px"><i class="fa fa-plus fa-1x"></i> &nbsp;ADD</button></a>                                            
                                                </div>
                                                <?PHP
                                            } else {
                                                ?>
                                                <div class="col-md-12">
                                                    <h4 style="color: red; text-align: center;"><em>****No data available***</em></h4>
                                                    <hr>
                                                </div>
                                                <div class="col-md-12" style=" padding: 25px">
                                                    <a href="addmember.php"><button class="btn btn-primary" type="button" style=" width: 150px;background-color: green; float: right; height: 45px"><i class="fa fa-plus fa-1x"></i> &nbsp;ADD</button></a>                                            
                                                </div>
                                                <?PHP
                                            }
                                            ?>  
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#tableActive6").addClass("active");
            $("#tableActive6").parent().parent().addClass("treeview active");
            $("#tableActive6").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>