<?php
ob_start();
include_once("../../config.php");
$id = $_GET['id'];
$result = $mysqli->query("SELECT * from member where memId= '$id' ");
while ($res = $result->fetch_object()) {
    $memId = $res->memId;
    $name = $res->memName;
    $img = $res->pimg;
    $role = $res->memRole;
    $dis = $res->memDis;
    $google = $res->memGoogle;
    $twiter = $res->memTwiter;
    $facebook = $res->memFacebook;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <?php include '../layout/header_script.php'; ?>
        <style>
            table, td, th {
                border: 1px solid black;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th {
                background-color: rgb(0, 70, 126);
                color: white;
                height: 50px;
                padding: 15px;
                text-align: left;
            }
            tr:nth-child(odd) {
                background-color:rgb(255, 210, 0);
                color: rgb(0, 70, 126);
            }

            td {
                padding: 15px;
                vertical-align: top;
                text-align: center;
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                text-align: center;
            }
            .image-preview-input {
                position: relative;
                overflow: hidden;
                margin: 0px;    
                color: #333;
                background-color: #fff;
                border-color: #ccc;    
            }
            .image-preview-input input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                padding: 0;
                font-size: 20px;
                cursor: pointer;
                opacity: 0;
                filter: alpha(opacity=0);
            }
            .image-preview-input-title {
                margin-left:2px;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php include '../layout/header.php'; ?>
            <?php include '../layout/sidebar.php'; ?>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-group">
                                <div class="panel panel-primary" style="border-color: #1E4770;">
                                    <div class="panel-heading panel-style" style=" background-color: #1E4770;"><i class="fa fa-edit"></i>&nbsp;UPDATE MEMBER</div>
                                    <div class="panel-body">
                                        <div class="row"> 
                                            <div class="col-md-8">
                                                <form action="../updateall.php" method="post" enctype="multipart/form-data">

                                                    <table align="right">
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <?php
                                                                if (isset($_GET['sms2'])) {
                                                                    ?>
                                                                    <div class="alert alert-warning" role="alert">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <strong>SORRY!</strong> your file is too large.Size must be less than 2mb!!!
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                                if (isset($_GET['sms3'])) {
                                                                    ?>
                                                                    <div class="alert alert-warning" role="alert">
                                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <strong>SORRY!</strong> only JPG, JPEG, PNG & GIF files are allowed.!!!
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <div>
                                                                    <input type="hidden" name="memId" value='<?php echo $memId; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="memName"><i class="fa fa-user"></i>&nbsp; NAME <b style="color:red;">*</b> </label>
                                                                    <input type="text" name="memName" value='<?php echo $name; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;"required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row"> 
                                                            <div class="col-md-6">  
                                                                <!-- image-preview-filename input [CUT FROM HERE]-->
                                                                <div>
                                                                    <label for="picture"><i class="fa fa-camera"></i>&nbsp; PICTURE</label>
                                                                    <div class="input-group image-preview">
                                                                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                                        <span class="input-group-btn">
                                                                            <!-- image-preview-clear button -->
                                                                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                                                <span class="glyphicon glyphicon-remove"></span> Clear
                                                                            </button>
                                                                            <!-- image-preview-input -->
                                                                            <div class="btn btn-default image-preview-input">
                                                                                <span class="glyphicon glyphicon-folder-open"></span>
                                                                                <span class="image-preview-input-title">Browse</span>
                                                                                <input type="file" accept="image/jpg,image/png, image/jpeg, image/gif" name="pimg" id="img"/> <!-- rename it -->
                                                                            </div>
                                                                        </span>
                                                                    </div><!-- /input-group image-preview [TO HERE]--> 
                                                                </div>
                                                                <?php
                                                                if (isset($_GET['error'])) {
                                                                    echo "<h4 style='color:red'><strong>*Invalid Picture Format</strong><h4>";
                                                                }
                                                                ?>
                                                                <script>
                                                                    $(document).on('click', '#close-preview', function () {
                                                                        $('.image-preview').popover('hide');
                                                                        // Hover befor close the preview
                                                                        $('.image-preview').hover(
                                                                                function () {
                                                                                    $('.image-preview').popover('show');
                                                                                },
                                                                                function () {
                                                                                    $('.image-preview').popover('hide');
                                                                                }
                                                                        );
                                                                    });

                                                                    $(function () {
                                                                        // Create the close button
                                                                        var closebtn = $('<button/>', {
                                                                            type: "button",
                                                                            text: 'x',
                                                                            id: 'close-preview',
                                                                            style: 'font-size: initial;',
                                                                        });
                                                                        closebtn.attr("class", "close pull-right");
                                                                        // Set the popover default content
                                                                        $('.image-preview').popover({
                                                                            trigger: 'manual',
                                                                            html: true,
                                                                            title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
                                                                            content: "There's no image",
                                                                            placement: 'right'
                                                                        });
                                                                        // Clear event
                                                                        $('.image-preview-clear').click(function () {
                                                                            $('.image-preview').attr("data-content", "").popover('hide');
                                                                            $('.image-preview-filename').val("");
                                                                            $('.image-preview-clear').hide();
                                                                            $('.image-preview-input input:file').val("");
                                                                            $(".image-preview-input-title").text("Browse");
                                                                        });
                                                                        // Create the preview image
                                                                        $(".image-preview-input input:file").change(function () {
                                                                            var img = $('<img/>', {
                                                                                id: 'dynamic',
                                                                                width: 250,
                                                                                height: 200
                                                                            });
                                                                            var file = this.files[0];
                                                                            var reader = new FileReader();
                                                                            // Set preview image into the popover data-content
                                                                            reader.onload = function (e) {
                                                                                $(".image-preview-input-title").text("Change");
                                                                                $(".image-preview-clear").show();
                                                                                $(".image-preview-filename").val(file.name);
                                                                                img.attr('src', e.target.result);
                                                                                $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
                                                                            }
                                                                            reader.readAsDataURL(file);
                                                                        });
                                                                    });

                                                                </script>
                                                            </div>
                                                        </div>     
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="role"> DESIGNATION <b style="color:red;">*</b> </label>
                                                                    <input type="text" name="memRole" value='<?php echo $role; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;" required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="discription"><i class="fa fa-info-circle"></i>  DISCRIPTION </label>
                                                                    <textarea name="memDis" rows="5" cols="30" style="width:100%" ><?php echo $dis; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>     
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="google"><i class="fa fa-google-plus"></i>  GOOGLE </label>
                                                                    <input type="text" name="memGoogle" value='<?php echo $google; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="twiter"><i class="fa fa-instagram"></i>  INSTAGRAM </label>
                                                                    <input type="text" name="memTwiter" value='<?php echo $twiter; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-6">  
                                                                <div>
                                                                    <label for="facebook"><i class="fa fa-facebook"></i>  FACEBOOK </label>
                                                                    <input type="text" name="memFacebook" value='<?php echo $facebook; ?>' style="width:100%;padding: 12px 20px;margin: 8px 0;display: inline-block;border: 1px solid #ccc;box-sizing: border-box;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                        <tr>
                                                        <div class="row">
                                                            <div class="col-md-3">  
                                                                <a href="memberlist.php"><button class="btn btn-primary" type="button" style="width:100%; background-color: #1E4770;"><i class="fa fa-arrow-left fa-1x"></i> BACK TO LIST</button></a>&nbsp;
                                                            </div>
                                                            <div class="col-md-3">  
                                                                <button type="submit" name="memupdate" class="btn btn-default" style="width:100%; background-color: green; color: white"><strong>UPDATE MEMBER</strong></button><br><br><br>
                                                            </div>
                                                        </div>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </div>
                                            <div class="col-md-4">
                                                <table>
                                                    <?php
                                                    include_once("../../config.php");
                                                    $result = $mysqli->query("SELECT * FROM member where memId= '$id'");
                                                    ?>
                                                    <tr>
                                                        <th>PICTURE</th>
                                                    </tr>
                                                    <?php while ($res = $result->fetch_object()) { ?>
                                                        <tr>
                                                            <td><img src = "<?= $baseUrl ?>admin/upload/<?php echo $res->pimg ?>" width = "100px" height = "100px"></td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php include '../layout/footer.php'; ?>
        </div>
        <?php include '../layout/footer_script.php'; ?>
        <script type="text/javascript">
            $("#dashActive6").addClass("active");
            $("#dashActive6").parent().parent().addClass("treeview active");
            $("#dashActive6").parent().addClass("in");
        </script>
    </body>
</html>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>