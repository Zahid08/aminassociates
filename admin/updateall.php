<?php

// including the database connection file
include_once("../config.php");
$sms = $sms1 = $sms2 = $sms3 = "";
$msg = "";
$s = "";
////////////////////////////PROFILE UPDATE ALRIGHT//////////////////////////////////////////////
if (isset($_POST['profileupdate'])) {
    $id = $_POST['adminId'];
    $adminName = $_POST['adminName'];
    $adminEmail = $_POST['adminEmail'];
    $adminPhn = $_POST['adminPhn'];
    $pimg = $_FILES['pimg']['name'];
    $target_dir = __DIR__ . "/upload/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 1000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 3;
        }
// Check if $uploadOk is set to 0 by an error
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/upload/$pimg");
            $mysqli->query("UPDATE admin SET adminName='$adminName', adminEmail='$adminEmail', adminPhn='$adminPhn', adminPic='$pimg' WHERE adminId='$id' ");
            header("Location: ./dashboard/profile.php?msg");
        }
        if ($uploadOk == 2) {
            header("Location: ./dashboard/editprofile.php?sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./dashboard/editprofile.php?sms3");
        }
    } elseif (empty($pimg)) {
        $mysqli->query("UPDATE admin SET adminName='$adminName', adminEmail='$adminEmail', adminPhn='$adminPhn' WHERE adminId='$id' ");
        header("Location: ./dashboard/profile.php?msg");
    }
}
////////////////////////////////////////PASSWORD UPDATE/////////////////////////////////////////
$pw = $mysqli->query("SELECT * FROM admin");
if (isset($_POST['passupdate'])) {
    $id = $_POST['adminId'];
    $oldpass = $_POST['oldpass'];
    $newpass = $_POST['newpass'];
    $cnfirmpass = $_POST['cnfirmpass'];
    $flag = 0;
    while (($res = $pw->fetch_object())) {
        if ((md5($oldpass) == $res->adminPass) && ($newpass == $cnfirmpass)) {
            $flag = 1;
        }
        $newpass= md5($newpass);
        if ($flag == 1) {
            $mysqli->query("UPDATE admin SET adminPass='$newpass' WHERE adminId='$id' ");
            header("Location: ./dashboard/profile.php?smsp");
        } else {
            header("Location: ./dashboard/editpassword.php?msg");
        }
    }
}
////////////////////////////////////////MESSAGE REPLY///////////////////////////////////////////////////////////////////
if (isset($_POST['msgupdate'])) {
    $to = $_POST['msgEmail'];
    $subject = $_POST['msgSub'];
    $msg = $_POST['msg'];
    $headers = 'From: magicshakeOL@gmail.com' . "\r\n" .
            'Reply-To: magicshakeOL@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $msg, $headers);
    header("Location: dashboard/msg.php?sms");
}
/////////////////////////////////BRAND PARTNER UPDATE ALRIGHT/////////////////////////////////////////
if (isset($_POST["brandupdate"])) {
    $id = $_POST['brandId'];
    $brandName = $_POST['brandName'];
    $brandUrl = $_POST['brandUrl'];
    $pimg = ($_FILES['pimg']['name']);
    $target_dir = __DIR__ . "/upload/brand/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/upload/brand/$pimg");
            $mysqli->query("UPDATE brand SET brandName='$brandName', brandUrl='$brandUrl', pimg='$pimg' WHERE brandId='$id' ");
            header("Location: ./brand/brandlist.php?msg");
        }
        if ($uploadOk == 2) {
            header("Location: ./brand/editbrand.php?id=" . $id . "&sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./brand/editbrand.php?id=" . $id . "&sms3");
        }
    } else {
        $mysqli->query("UPDATE brand SET brandName='$brandName', brandUrl='$brandUrl' WHERE brandId='$id' ");
        header("Location: ./brand/brandlist.php?msg");
    }
}
/////////////////////////////////CONTACT UPDATE ALRIGHT/////////////////////////////////////////
if (isset($_POST["contactupdate"])) {
    $id = $_POST['contactId'];
    $contactName = $_POST['contactName'];
    $contactRole = $_POST['contactRole'];
    $contactPhone = $_POST['contactPhone'];
    $mysqli->query("UPDATE contact SET contactName='$contactName', contactRole='$contactRole',contactPhone='$contactPhone' WHERE contactId='$id' ");
    header("Location: ./contact/contactlist.php?msg");
}
/////////////////////////////////FACTORY IMAGE UPDATE ALRIGHT/////////////////////////////////////////
if (isset($_POST["facupdate"])) {
    $id = $_POST['facId'];
    $facName = $_POST['facName'];
    $facDis = $_POST['facDis'];
    $pimg = ($_FILES['pimg']['name']);
    $target_dir = __DIR__ . "/upload/gallery/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/upload/gallery/$pimg");
            $mysqli->query("UPDATE factory SET facName='$facName', pimg='$pimg', facDis='$facDis' WHERE facId='$id' ");
            header("Location: ./factory/imagelist.php?msg");
        }
        if ($uploadOk == 2) {
            header("Location: ./factory/editimage.php?id=" . $id . "&sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./factory/editimage.php?id=" . $id . "&sms3");
        }
    } else {
        $mysqli->query("UPDATE factory SET facName='$facName', facDis='$facDis' WHERE facId='$id' ");
        header("Location: ./factory/imagelist.php?msg");
    }
}
////////////////////////////MEMBER UPDATE ALRIGHT//////////////////////////////////////////////
if (isset($_POST['memupdate'])) {
    $id = $_POST['memId'];
    $memName = $_POST['memName'];
    $memRole = $_POST['memRole'];
    $memDis = $_POST['memDis'];
    $memGoogle = $_POST['memGoogle'];
    $memTwiter = $_POST['memTwiter'];
    $memFacebook = $_POST['memFacebook'];
    $pimg = $_FILES['pimg']['name'];
    $target_dir = __DIR__ . "/upload/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/upload/$pimg");
            $mysqli->query("UPDATE member SET memName='$memName', pimg='$pimg', memRole='$memRole', memDis='$memDis',  memGoogle='$memGoogle', memTwiter='$memTwiter',memFacebook='$memFacebook' WHERE memId='$id' ");
            header("Location: ./member/memberlist.php?msg");
        }
        if ($uploadOk == 2) {
            header("Location: ./member/editmember.php?id=" . $id . "&sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./member/editmember.php?id=" . $id . "&sms3");
        }
    } else {
        $mysqli->query("UPDATE member SET memName='$memName', memRole='$memRole', memDis='$memDis',  memGoogle='$memGoogle', memTwiter='$memTwiter',memFacebook='$memFacebook' WHERE memId='$id' ");
        header("Location: ./member/memberlist.php?msg");
    }
}
/////////////////////////////////////PRODUCT UPDATE ALRIGHT///////////////////////////////////////////////////////
if (isset($_POST['proupdate'])) {
    $id = $_POST['pid'];
    $proname = $_POST['proname'];
    $pname = $_POST['pname'];
    $pdiscrip = $_POST['pdiscrip'];
    $pstutus = $_POST['stutus'];
    $pimg = $_FILES['pimg']['name'];
    $target_dir = __DIR__ . "/upload/product/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    $x = $mysqli->query("SELECT * from product");
    $count = 0;
    while ($res = $x->fetch_object()) {
        if (($proname === $res->ptype) && ($pname === $res->pname) && ($id != $res->pid)) {
            $count++;
        }
    }
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0 && $count < 1) {
            move_uploaded_file($temp, __DIR__ . "/upload/product/$pimg");
            $mysqli->query("UPDATE product SET ptype='$proname', pname='$pname', pimg='$pimg', pdiscrip='$pdiscrip', pstutus = '$pstutus' WHERE pid='$id' ");
            header("Location: ./product/productlist.php?msg");
        } elseif ($uploadOk == 0 && $count >= 1) {
            header("Location: ./product/editproducts.php?id=" . $id . "&s");
        } elseif ($uploadOk == 2) {
            header("Location: ./product/editproducts.php?id=" . $id . "&sms2");
        } elseif ($uploadOk == 3) {
            header("Location: ./product/editproducts.php?id=" . $id . "&sms3");
        }
    } else {
        if ($count < 1) {
            $mysqli->query("UPDATE product SET ptype='$proname', pname='$pname', pdiscrip='$pdiscrip', pstutus = '$pstutus' WHERE pid='$id' ");
            header("Location: ./product/productlist.php?msg");
        } elseif ($count >= 1) {
            header("Location: ./product/editproducts.php?id=" . $id . "&s");
        }
    }
}
////////////////////////////////////////PRODUCT NAME UPDATE//////////////////////////////////////////////////
$product = $mysqli->query("SELECT proname from productname");
if (isset($_POST['pnameupdate'])) {
    $flag = 0;
    $id = $_POST['proid'];
    $proname = $_POST['proname'];
    while ($res = $product->fetch_object()) {
        if (($proname === $res->proname)) {
            $flag = 1;
        }
    }
    if ($flag == 0) {
//updating the table
        $mysqli->query("UPDATE productname SET proname='$proname' WHERE proid='$id' ");
        header("Location: ./product/productnamelist.php?sms");
    } else {
        header("Location: ./product/editproductsname.php?id=" . $id . "&msg");
    }
}
/////////////////////////////////SERVICE UPDATE ALRIGHT/////////////////////////////////////////

if (isset($_POST['serupdate'])) {
    $id = $_POST['serviceId'];
    $serviceName = $_POST['serviceName'];
    $serviceDis = $_POST['serviceDis'];
    $stutus = $_POST['stutus'];
    $pimg = $_FILES['pimg']['name'];
    $target_dir = __DIR__ . "/upload/service/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $uploadOk = 0;
    $flag = 0;
    $service = $mysqli->query("SELECT * from service");
    while ($res = $service->fetch_object()) {
        if (($serviceName === $res->serviceName) && ($id != $res->serviceId)) {
            $flag++;
        }
    }
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0 && $flag < 1) {
            move_uploaded_file($temp, __DIR__ . "/upload/service/$pimg");
            $mysqli->query("UPDATE service SET serviceName='$serviceName', pimg='$pimg', serviceDis='$serviceDis', serviceStutus = '$stutus' WHERE serviceId='$id' ");
            header("Location: ./service/servicelist.php?msg");
        }
        if ($uploadOk == 0 && $flag >= 1) {
            header("Location: ./service/editservice.php?id=" . $id . "&sms1");
        }
        if ($uploadOk == 2) {
            header("Location: ./service/editservice.php?id=" . $id . "&sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./service/editservice.php?id=" . $id . "&sms3");
        }
    } else {
        if ($flag < 1) {
            //echo $flag;
            //exit();
            $mysqli->query("UPDATE service SET serviceName='$serviceName', serviceDis='$serviceDis', serviceStutus = '$stutus' WHERE serviceId='$id' ");
            header("Location: ./service/servicelist.php?msg");
        } else {
            //echo $flag;
            //exit();
            header("Location: ./service/editservice.php?id=" . $id . "&sms1");
        }
    }
}
////////////////////////////SLIDER UPDATE ALRIGHT//////////////////////////////////////////////
if (isset($_POST['sliupdate'])) {
    $id = $_POST['sliderId'];
    $sliderName = $_POST['sliderName'];
    $pimg = $_FILES['pimg']['name'];
    $target_dir = __DIR__ . "/upload/slider/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/upload/slider/$pimg");
            $mysqli->query("UPDATE slider SET sliderName='$sliderName', pimg='$pimg'  WHERE sliderId='$id' ");
            header("Location: ./slider/sliderlist.php?msg");
        }
        if ($uploadOk == 2) {
            header("Location: ./slider/editslider.php?id=" . $id . "&sms2");
        }
        if ($uploadOk == 3) {
            header("Location: ./slider/editslider.php?id=" . $id . "&sms3");
        }
    } else {
        $mysqli->query("UPDATE slider SET sliderName='$sliderName' WHERE sliderId='$id' ");
        header("Location: ./slider/sliderlist.php?msg");
    }
}
?>