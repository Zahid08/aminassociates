<?php

include_once("../../config.php");
$sms = "";
$msg = "";
if (isset($_POST["submit"])) {
    $sliderName = $_POST['sliderName'];
    $pimg = ($_FILES['pimg']['name']);
    $target_dir = __DIR__ . "/upload/slider/";
    $target_file = $target_dir . basename($_FILES["pimg"]["name"]);
    $uploadOk = 0;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    if (!empty($pimg)) {
// Check file size
        if ($_FILES["pimg"]["size"] / 1024 > 2000) {
            $uploadOk = 2;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $uploadOk = 3;
        }
// Check if $uploadOk is set to 0 by an error
        $size = ($_FILES['pimg']['size']);
        $temp = ($_FILES['pimg']['tmp_name']);
        $pimg = uniqid() . ($_FILES['pimg']['name']);
        if ($uploadOk == 0) {
            move_uploaded_file($temp, __DIR__ . "/../upload/slider/$pimg");
            $mysqli->query("INSERT INTO slider(sliderName,pimg) VALUES('$sliderName','$pimg')");
            header("Location: addslider.php?sms");
        }
        if ($uploadOk == 2) {
            header("Location: addslider.php?sms2");
        }
        if ($uploadOk == 3) {
            header("Location: addslider.php?sms3");
        }
    } else {
        header("Location: addslider.php?msg");
    }
}
?>