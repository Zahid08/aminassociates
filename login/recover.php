<?php
include_once("../config.php");
$pw = $mysqli->query("SELECT * FROM admin");
$res = $pw->fetch_object();
$adminId = $res->adminId;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
                        <span class="login100-form-title-1">
                            Change Password
                        </span>
                    </div>
                    <form class="login100-form validate-form" action="log.php" method="post">
                        <?php if (isset($_GET['recovery'])) { ?>
                            <div class="alert alert-danger">
                                <strong>Failed!</strong> Password formate is wrong!!
                            </div>
                        <?php } ?>

                        <div>
                            <label style="color: #0066cc">(**Password must contain minimum one letter , one number and minimum length is six**)</label>
                            <br>
                        </div>
                        <input type="hidden" name="adminId" value="<?php echo $adminId; ?>"/>                            
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Password is required">
                            <label class="label-input100" style="color: #0066cc">New Password</label>
                            <input class="input100" type="password" name="newPass" placeholder="Enter new password" pattern="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$" required/>
                            <span class="validity"></span>
                            <span class="focus-input100"></span>
                        </div>
                        <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                            <span class="label-input100" style="color: #0066cc">Confirm Password</span>
                            <input class="input100" type="password" name="confirmPass" placeholder="Confirm new password" pattern="^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$" required/>
                            <span class="validity"></span>
                            <span class="focus-input100"></span>
                        </div>

                        <div class="row">
                            <div class="col-md-12" style="align: right;">
                                <input type="submit" name="password" value="Submit" class="login100-form-btn" /> 
                            </div>
                        </div>
                        <br><br><br>
                        <div class="flex-sb-m w-full p-b-30">
                            <div>
                                <a href="<?= $baseUrl ?>" class="txt1" style="color: #0066cc"><i class="fa fa-arrow-left"></i>&nbsp;Back To Home</a>   
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>   
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <!--===============================================================================================-->
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <!--===============================================================================================-->
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <!--===============================================================================================-->
        <script src="js/main.js"></script>

    </body>
</html>