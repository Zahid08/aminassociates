<?php
include_once("config.php");
?>
<!DOCTYPE html>
<html lang="en-US" >

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>ABOUT &#8211; Amin Associates Limited � Bangladesh</title>
        <meta name='robots' content='noindex,follow' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="assets/css/art.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='siteorigin-panels-front-css'  href='assets/css/front.css?ver=2.4.9' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-plugin-css-css'  href='assets/css/plugin.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-style-css'  href='assets/css/style.css' type='text/css' media='all' />
        <style>
            input[type=text], input[type=password],input[type=email] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            /* Set a style for all buttons*/ 


            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 20%;
                border-radius: 50%;
            }

            .containerlogin {
                padding: 16px;
                width: 100%;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }
            .modal-dialog {
                margin-top: 88px;
            }
            /* The Modal (background) */
            .modallogin {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modallogin-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 50%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
                to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
                to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
        </style>
    </head>

    <body class="page page-id-1686 page-template-default siteorigin-panels">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- MAIN WRAPPER -->
        <div id="main-wrapper" class="animsition clearfix">
            <header id="masthead" class="site-header navbar-fixed-top">
                <div class="header-navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                        
                            <div class="logo navbar-brand">
                                <div class="logo-image">
                                    <a href="<?= $baseUrl ?>"><img src="image/logo-1.png" class="image-logo" alt="logo" /></a>
                                </div>
                            </div><!-- end logo -->
                            <nav id="primary-navigation" class="site-navigation navbar-collapse collapse" role="navigation">
                                <div class="nav-menu">
                                    <ul id="menu-all-pages" class="menu">
                                        <li><a href="<?= $baseUrl ?>">HOME</a></li>
                                        <li><a href="<?= $baseUrl ?>products.php">PRODUCTS</a></li>
                                        <li><a href="<?= $baseUrl ?>gallery.php">FACTORY GALLERY</a></li>
                                        <li class="active"><a href="<?= $baseUrl ?>about.php">ABOUT</a></li>
                                        <li><a href="<?= $baseUrl ?>contact.php">CONTACT</a></li>
                                        <li><a href="<?= $baseUrl ?>#appointment">Make an Appoinment</a></li>
                                    </ul>
                                </div><!-- end nav-menu -->
                            </nav><!-- end #primary-navigation -->
                            <div class="appoinment-header">
                                <a href="<?= $baseUrl ?>login/login.php"  target="_blank" class="btn btn-md btn-default" style="width:auto; background-color: #103b7a; color: #f9c937">Login</a>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end container-fluid -->
                </div><!-- end header-navigation -->
            </header><!-- end #masthead -->
            <div id="content-wrapper" class="wrapper with-margin">	
                <article  id="page-1686" class="page container post-1686 type-page status-publish hentry">
                    <div  style="padding: 80px;background-image: url(image/05/aabg_2.jpg);background-size: cover;" data-stretch-type="full" >                              
                        <div class="white-box-new panel-widget-style" style="padding: 35px 0px 40px 40px;opacity: 0.9;background-color:#ffffff;color: #ffffff;" >  
                            <div class="the-title text-center dark">
                                <h3>ABOUT AMIN ASSOCIATES</h3>
                            </div>
                            <div class="about-content">
                                <p style="text-align: justify">
                                    The Amin Associates Ltd. is a symbol of efficiency, reliability, experience and innovative ideas. We produce high quality Belts, Elastics, Twill Tapes, Hang Tags, Photo Inlayers, Woven Labels, Printed Labels, Metal Buckles/ D-rings, Sewing Thread, Jacquard Elastics, Jacquard Tapes, Cotton Laces, Bra &amp; Panty Laces, Gum Tape, Drawstrings &amp; Cord etc. Our products are trust worthy, hence we always maintain customers testing requirements before the bulk production. We always emphasize on the comfort of our customers, as we are well aware most of our products are used in Kids &amp; Children�s garments.
                                    <br />
                                    <br />
                                    We plan our production according to the market demand and can produce any quality provided by the buyer with the sufficient machine capacity. We maintain strict pre-caution to maintaining quality and produce them with zero defect efficiency.
                                    The raw materials we use are of A- grade &amp; Oekotex certified imported from abroad (China, Taiwan, Thailand &amp;Malaysia) and therefore excellent in quality. About color we can ensure you 100% yarn dyed without harmful reagents and objects.
                                    <br />
                                    <br />
                                    <strong>Amin Associates Ltd.</strong> ensures you the product�s high quality, productivity, shades, strength, length, color etc.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                        
                        <?php
                        $result = $mysqli->query("SELECT * FROM about");
                        while ($res = $result->fetch_object()) {
                            ?>
                            <img src="<?= $baseUrl ?>admin/upload/<?php echo $res->pimg ?>" alt="<?php echo $res->pimg ?>" style="width:1200px; height:460px;"/>
                        <?php }
                        ?>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Factory Description</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">
                                We have well decorated and furnished factory settings with 223 employees at Uttara, Abdullahpur. All of them are skilled at their respective areas. The total shed area is 25,120 sq ft. in where we settled our one stop service for 24 hours. We give ourcommitment according to our capacity and deliver the products as per schedule. Our quality controllers are skilled in their position and committed to serve their best for buyer�s satisfaction. A small office maintained by our factory Manager in where he records&amp; operates our daily affairs.  At our store we always ensure quality raw materials with perfect placement. The store made for the output and packing is also mentionable here because there we recheck finally for the delivery. 
                                <br />
                                <br />
                            </p>
                        </div>
                        <div class="panel-grid" id="pg-1684-1" >
                            <div class="siteorigin-panels-stretch panel-row-style" data-stretch-type="full" >
                                <div class="so-panel widget widget_service-widget panel-last-child" id="panel-1684-1-0-1" data-index="2">
                                    <div class="so-widget-service-widget so-widget-service-widget-base">
                                        <?php
                                        $i = 0;
                                        $result = $mysqli->query("SELECT * FROM service where serviceStutus='Active'");
                                        while ($res = $result->fetch_object()) {
                                            if ($i == 0 || $i % 2 == 0) {
                                                echo '<div class="row">';
                                            }
                                            $i++;
                                            echo '<div class="col-md-6">';
                                            ?>
                                            <div class="service-col" style="width: 100%">
                                                <h3> 
                                                    <?php echo $res->serviceName; ?>
                                                </h3>
                                                <div class="service-content">
                                                    <?php echo $res->serviceDis; ?>
                                                    <br>
                                                    <a data-toggle="modal" href="#myModal<?php echo $res->serviceId; ?>" style="color: #0099cc;">View Details</a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="myModal<?php echo $res->serviceId; ?>" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" style="align:right;">&times;</button>
                                                                    <h3 class="modal-title">
                                                                        <?php echo $res->serviceName; ?>
                                                                    </h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>
                                                                        <?php echo $res->serviceDis; ?>
                                                                    </p>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal" style=" background-color: #103b7a; color: #f9c937 ">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <figure>
                                                    <img src="<?= $baseUrl ?>admin/upload/service/<?php echo $res->pimg ?>" alt="<?php echo $res->serviceName ?>" />
                                                </figure>
                                            </div>
                                            <?php
                                            echo '</div>';
                                            if ($i % 2 == 0) {
                                                echo '</div>';
                                            } else {
                                                continue;
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Marketing</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">
                                We have experienced teams for marketing, who visit the factories per their requirements and visit customer�s office and collect samples. They send these samples to our factories to develop sample as per customer�s requirement. These samples are sent to the customers for their quality satisfaction and finally approval of the orders. This service is provided at 24 to 48 hours delivery of the goods and maintain customer relation by using mobile phone, fax, e-mail, and when necessary the converse directly.
                                <br />
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Quality Control</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">
                                Quality control covers all the work of controlling efficiency for the purpose of guarantying the required quality of the buyers and in order to optimize the cost involvement of all the process needed to manufacture the product. It covers the stages starting with procurement of quality yarn, dyes, chemicals, and laboratory testing for color, shades, color fastness, process control, inspection and packing and ending with delivery.
                                <br />
                                We have well trained, skilled &amp; experienced teams who make our products different and helps to keep our commitment. By machinery process and our personal effort we set up for sample testing to prepare our products for manufacturing &amp; by turns to the final delivery. 
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Sample Development</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">
                                It is the key point of our starting business relationship. We take 3-7 days for any complete sample development. We provide our buyer�s free of cost samples and confirm right quality &amp; quantity at right time. We ensure that samples are made according to the client�s specification, as color, as shade etc. We also recreate and modify if needed for buyer�s styling &amp; cost purpose. 
                                <br />
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Design</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">We create designs according to our client�s specification that they provide us sketch or idea or sometimes physical samples. We research on basis and create the very best, imitate the original.
                                <br />
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Raw Materials</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify">We have strong source for raw materials both in local &amp; foreign. We always ensure quality &amp; standard raw materials and select the apt raw from our wide range of collection for starting the sampling process &amp; later the same for bulk after approval. 
                                <br />
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Employee</h3>
                        </div>
                        <div class="about-content">
                            <b>Factory Employee (Employment Generation) :</b>  223 Persons.
                            <br /><br />
                            <b>Employee Safety, Health &amp; Hygienic System:</b>
                            <br />
                            <ol>
                                <li>Free Medical Service.</li>
                                <li>Available FIRSTAID box with medicine.</li>
                                <li>Enough fire extinguisher.</li>
                                <li>Trained fire fighting team.</li>
                                <li>Healty toilets and urinals.</li>
                                <li>Washing facilities etc.</li>
                            </ol>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Managing Committee</h3>
                        </div>
                        <div class="about-content">
                            <p style="text-align: justify"> 
                                Our directors are from various reendowed companies and have their own experience in the respective field. All of we are about to employ our own intellect and experience to innovate a new outlook and a new path for the next generation business. We have strong capability and belief that we will be able to fulfill any need of the buyers and have the grudge to satisfy them by providing our service.  
                            </p>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Achievements &amp; Certificates</h3>
                        </div>
                        <div class="about-content">
                            <ul>
                                <li>(QC100 ,For Quality &amp; Service) - Gold Category , London-2012.</li>
                                <li>OEKO TexStandard 100.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                        <div class="the-title text-center dark">
                            <h3>Banking Details</h3>
                        </div>
                        <div class="container">
                            <div class="row col-md-12">
                                <div class="col-sm-6">
                                    <p><strong><i class="fa fa-university" aria-hidden="true"></i> Export Import Bank of Bangladesh Limited</strong>
                                        <br>Gulshan Branch
                                        <br>A/C: 0711100209149
                                        <br>Swift Code: EXBKBDDH007
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p><strong><i class="fa fa-university" aria-hidden="true"></i> Jamuna Bank Limited</strong>
                                        <br>Uttara Branch
                                        <br>A/C: 0370210007658
                                        <br>Swift Code: JAMUBDDH054
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p><strong><i class="fa fa-university" aria-hidden="true"></i> Al- ArafahIslami Bank Limited</strong>
                                        <br>Dhanmondi Branch
                                        <br>A/C: 0311020012606
                                        <br>Swift Code: AL-ARBDDH084
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p><strong><i class="fa fa-university" aria-hidden="true"></i> Woori Bank</strong>
                                        <br>Uttara Branch
                                        <br>A/C: 964000467
                                        <br>Swift Code: HVBKBDDHXXX
                                    </p>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                        <div class="so-widget-title-widget so-widget-title-widget-base">
                            <div class="the-title text-center dark">
                                <h3>MANAGEMENT TEAM</h3>
                            </div>
                            <div class="row">
                                <?php
                                $result = $mysqli->query("SELECT * FROM member");
                                while ($res = $result->fetch_object()) {
                                    ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="team-member">
                                            <div class="team-img">
                                                <img src="<?= $baseUrl ?>admin/upload/<?php echo $res->pimg ?>" alt="<?php echo $res->memName ?>" class="img-responsive" style="width:360px;height:310px;">
                                            </div>
                                            <div class="team-hover">
                                                <div class="desk">
                                                    <h4>Hi There !</h4>
                                                    <p><?php echo $res->memDis ?></p>
                                                </div>
                                                <div class="s-link">
                                                    <a href="<?php echo $res->memFacebook ?>"><i class="fa fa-facebook"></i></a>
                                                    <a href="<?php echo $res->memTwiter ?>"><i class="fa fa-instagram"></i></a>
                                                    <a href="<?php echo $res->memGoogle ?>"><i class="fa fa-google-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="team-title">
                                            <h5><?php echo $res->memName ?></h5>
                                            <span><?php echo $res->memRole ?></span>
                                        </div>
                                    </div>                            
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </article><!-- #page1686 -->
            </div><!-- wrapper -->
        </div>
        <!-- #main wrapper end-->
        <div class="footer-credit">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copy">Amin Associates 2016 - Powered by <a href="http://www.olivineltd.com/"> OLIVINE LIMITED</a></p>
                    </div><!-- end column -->
                    <div class="col-md-6">
                        <ul class="list-socmed">
                            <li class="twitter soc-icon"><a href="http://twitter.com/#" class="fa fa-twitter"></a></li>
                            <li class="google soc-icon"><a href="http://google.com/#" class="fa fa-google-plus"></a></li>
                            <li class="facebook soc-icon"><a href="http://facebook.com/#" class="fa fa-facebook"></a></li>
                            <li class="linkedin soc-icon"><a href="http://linkedin.com/#" class="fa fa-linkedin"></a></li>
                            <li class="pinterest soc-icon"><a href="http://pinterest.com/#" class="fa fa-pinterest"></a></li>
                            <li class="youtube soc-icon"><a href="https://www.youtube.com/" class="fa fa-youtube"></a></li>
                            <li class="instagram soc-icon"><a href="https://instagram.com/" class="fa fa-instagram"></a></li>
                        </ul>
                    </div><!-- end column -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end footer-credit -->
        <script>
            $(document).ready(function () {
                // Add smooth scrolling to all links in navbar + footer link
                $(".navbar a, footer a[href='#myPage']").on('click', function (event) {
                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 900, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    } // End if
                });

                $(window).scroll(function () {
                    $(".slideanim").each(function () {
                        var pos = $(this).offset().top;

                        var winTop = $(window).scrollTop();
                        if (pos < winTop + 600) {
                            $(this).addClass("slide");
                        }
                    });
                });
            })
        </script>
        <script>
            $(document).ready(function () {
                $("#myBtn").click(function () {
                    $("#myModal").modal();
                });
            });
        </script>
    </body>
</html>