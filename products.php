<?php
include_once("config.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>PRODUCTS &#8211; Amin Associates Limited Bangladesh &#8211; Just another WordPress site</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/art.css">
        <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/modal.css">  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='siteorigin-panels-front-css'  href='<?= $baseUrl ?>assets/css/front.css?ver=2.4.9' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-plugin-css-css'  href='<?= $baseUrl ?>assets/css/plugin.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-style-css'  href='<?= $baseUrl ?>assets/css/style.css' type='text/css' media='all' />              
        <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/design.css">
        <style>
            input[type=text], input[type=password] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            /* Set a style for all buttons*/

            button:hover {
                opacity: 0.8;
            }

            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center theimage and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 20%;
                border-radius: 50%;
            }

            .containermodal {
                padding: 16px;
                width: 100%;
            }
            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modallogin {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }

            /* Modal Content/Box */
            .modallogin-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 50%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .closelogin {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .closelogin:hover,
            .closelogin:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
                to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
                to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
            .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%
            }
        </style>
    </head>
    <body class="page page-id-1686 page-template-default siteorigin-panels">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- MAIN WRAPPER -->
        <div id="main-wrapper" class="animsition clearfix">
            <header id="masthead" class="site-header navbar-fixed-top">
                <div class="header-navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                        
                            <div class="logo navbar-brand">
                                <div class="logo-image">
                                    <a href="<?= $baseUrl ?>"><img src="image/logo-1.png" class="image-logo" alt="logo" /></a>
                                </div>
                            </div><!-- end logo -->
                            <nav id="primary-navigation" class="site-navigation navbar-collapse collapse" role="navigation">
                                <div class="nav-menu">
                                    <ul id="menu-all-pages" class="menu">
                                        <li><a href="<?= $baseUrl ?>">HOME</a></li>
                                        <li class="active"><a href="<?= $baseUrl ?>products.php">PRODUCTS</a></li>
                                        <li><a href="<?= $baseUrl ?>gallery.php">FACTORY GALLERY</a></li>
                                        <li><a href="<?= $baseUrl ?>about.php">ABOUT</a></li>
                                        <li><a href="<?= $baseUrl ?>contact.php">CONTACT</a></li>
                                        <li><a href="<?= $baseUrl ?>#appointment">Make an Appoinment</a></li>
                                    </ul>
                                </div><!-- end nav-menu -->
                            </nav><!-- end #primary-navigation -->
                            <div class="appoinment-header">
                                <a href="<?= $baseUrl ?>login/login.php"  target="_blank" class="btn btn-md btn-default" style="width:auto; background-color: #103b7a; color: #f9c937">Login</a>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end container-fluid -->
                </div><!-- end header-navigation -->
            </header><!-- end #masthead -->
            <div id="content-wrapper" class="wrapper with-margin">
                <article  id="page-1684" class="page container post-1684 type-page status-publish hentry">
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 10px;" data-stretch-type="full">                    
                        <div class="so-widget-title-widget so-widget-title-widget-base">
                            <div id="myBtnContainer">
                                <div class="row">
                                    <?php
                                    if ($ruu = $mysqli->query("SELECT pid FROM product")) {

                                        /* determine number of rows result set */
                                        $r = $ruu->num_rows;

                                        $ruu->close();
                                    }if ($r > 0) {
                                        ?>
                                        <div class = "col-md-2" style = " padding: 5px;">
                                            <button class = "btn active" style = "width:200px; font-size: 11px;" onclick = "filterSelection('all')"> Show all</button>
                                        </div>
                                        <?php
                                    }
                                    $re = $mysqli->query("SELECT proname FROM productname");
                                    while ($res = $re->fetch_object()) {
                                        ?>
                                        <div class="col-md-2" style=" padding: 5px;">
                                            <button class="btn"  style="width:200px; font-size: 11px;" onclick="filterSelection('<?php echo $res->proname; ?>')"> <?php echo $res->proname; ?></button>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Portfolio Gallery Grid -->
                    <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                        <div class="so-widget-title-widget so-widget-title-widget-base">
                            <div class="row">
                                <?php
                                $result = $mysqli->query("SELECT * FROM product where pstutus='Active'");
                                while ($res = $result->fetch_object()) {
                                    ?>
                                    <div class="column <?php echo $res->ptype; ?>">
                                        <div class="content">
                                            <a data-toggle="modal" href="#myModal<?php echo $res->pid; ?>">
                                                <img src="<?= $baseUrl ?>admin/upload/product/<?php echo $res->pimg; ?>" alt="<?php echo $res->pname ?>" style="width:360px;height: 200px;"/>
                                            </a>
                                            <h4><?php echo $res->pname; ?></h4>
                                            <!--<p><?//php echo $res->pdiscrip; ?></p>-->
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $res->pid; ?>" role="dialog" style=" background-color: rgba(0,0,0,0.1);">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3 class="modal-title">
                                                                <?php echo $res->pname; ?>
                                                            </h3>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>
                                                            <center><img src="<?= $baseUrl ?>admin/upload/product/<?php echo $res->pimg; ?>" alt="<?php echo $res->pname ?>" style="width:400px;height: 250px"/></center>
                                                            <br>
                                                            <?php echo $res->pdiscrip; ?>
                                                            </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-default" data-dismiss="modal" style=" background-color: #103b7a; color: #f9c937; width: 20% ">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- END MAIN -->
                </article><!-- #page1644 -->
            </div>
        </div>
        <!--footer start -->
        <footer class="footer">
            <div class="footer-credit">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="copy">Amin Associates  2016 - Powered by <a href="http://www.olivineltd.com/"> OLIVINE LIMITED</a></p>
                        </div><!-- end column -->
                        <div class="col-md-6">
                            <ul class="list-socmed">
                                <li class="twitter soc-icon"><a href="http://twitter.com/#" class="fa fa-twitter"></a></li>
                                <li class="google soc-icon"><a href="http://google.com/#" class="fa fa-google-plus"></a></li>
                                <li class="facebook soc-icon"><a href="http://facebook.com/#" class="fa fa-facebook"></a></li>
                                <li class="linkedin soc-icon"><a href="http://linkedin.com/#" class="fa fa-linkedin"></a></li>
                                <li class="pinterest soc-icon"><a href="http://pinterest.com/#" class="fa fa-pinterest"></a></li>
                                <li class="youtube soc-icon"><a href="https://www.youtube.com/" class="fa fa-youtube"></a></li>
                                <li class="instagram soc-icon"><a href="https://instagram.com/" class="fa fa-instagram"></a></li>
                            </ul>
                        </div><!-- end column -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end footer-credit -->
        </footer>
        <!-- #main wrapper end-->
        <script>
            filterSelection("all")
            function filterSelection(c) {
                var x, i;
                x = document.getElementsByClassName("column");
                if (c == "all")
                    c = "";
                for (i = 0; i < x.length; i++) {
                    w3RemoveClass(x[i], "show");
                    if (x[i].className.indexOf(c) > -1)
                        w3AddClass(x[i], "show");
                }
            }
            function w3AddClass(element, name) {
                var i, arr1, arr2;
                arr1 = element.className.split(" ");
                arr2 = name.split(" ");
                for (i = 0; i < arr2.length; i++) {
                    if (arr1.indexOf(arr2[i]) == -1) {
                        element.className += " " + arr2[i];
                    }
                }
            }
            function w3RemoveClass(element, name) {
                var i, arr1, arr2;
                arr1 = element.className.split(" ");
                arr2 = name.split(" ");
                for (i = 0; i < arr2.length; i++) {
                    while (arr1.indexOf(arr2[i]) > -1) {
                        arr1.splice(arr1.indexOf(arr2[i]), 1);
                    }
                }
                element.className = arr1.join(" ");
            }
            // Add active class to the current button (highlight it)
            var btnContainer = document.getElementById("myBtnContainer");
            var btns = btnContainer.getElementsByClassName("btn");
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click", function () {
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    this.className += " active";
                });
            }
        </script>
        <script>
            $(document).ready(function () {
                $("#myBtn").click(function () {
                    $("#myModal").modal();
                });
            });
        </script>
    </body>
</html>
