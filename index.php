<?php
include_once("config.php");
if ($result = $mysqli->query("SELECT serviceId FROM service")) {

    /* determine number of rows result set */
    $row_cnt = $result->num_rows;

    /* close result set */
    $result->close();
}
if ($result = $mysqli->query("SELECT sliderId FROM slider")) {

    /* determine number of rows result set */
    $row_cnt1 = $result->num_rows;
    $c = $row_cnt1 - 1;
    /* close result set */
    $result->close();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>HOME &#8211; Amin Associates Limited Bangladesh &#8211; Just another WordPress site</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='siteorigin-panels-front-css'  href='<?= $baseUrl ?>assets/css/front.css?ver=2.4.9' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-plugin-css-css'  href='<?= $baseUrl ?>assets/css/plugin.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bengkel-style-css'  href='<?= $baseUrl ?>assets/css/style.css' type='text/css' media='all' />      
        <link rel="stylesheet" href="<?= $baseUrl ?>assets/css/art.css">
        <style>
            input[type=text], input[type=number],input[type=password],input[type=email] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }
            /* Extra styles for the cancel button */
            .cancelbtn {
                width: auto;
                padding: 10px 18px;
                background-color: #f44336;
            }

            /* Center the image and position the close button */
            .imgcontainer {
                text-align: center;
                margin: 24px 0 12px 0;
                position: relative;
            }

            img.avatar {
                width: 20%;
                border-radius: 50%;
            }

            .containerlogin {
                padding: 16px;
                width: 100%;
            }

            span.psw {
                float: right;
                padding-top: 16px;
            }

            /* The Modal (background) */
            .modallogin {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                padding-top: 60px;
            }
            .modal-dialog {
                margin-top: 88px;
            }

            /* Modal Content/Box */
            .modallogin-content {
                background-color: #fefefe;
                margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
                border: 1px solid #888;
                width: 50%; /* Could be more or less, depending on screen size */
            }

            /* The Close Button (x) */
            .close {
                position: absolute;
                right: 25px;
                top: 0;
                color: #000;
                font-size: 35px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: red;
                cursor: pointer;
            }

            /* Add Zoom Animation */
            .animate {
                -webkit-animation: animatezoom 0.6s;
                animation: animatezoom 0.6s
            }

            @-webkit-keyframes animatezoom {
                from {-webkit-transform: scale(0)} 
                to {-webkit-transform: scale(1)}
            }

            @keyframes animatezoom {
                from {transform: scale(0)} 
                to {transform: scale(1)}
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
            .team-title {
                position: static;
                padding: 20px;
                display: inline-block;
                letter-spacing: 2px;
                width: 100%;
            }
        </style>
    </head>
    <body class="page page-id-1686 page-template-default siteorigin-panels">
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- MAIN WRAPPER -->
        <div id="main-wrapper" class="animsition clearfix">
            <header id="masthead" class="site-header navbar-fixed-top">
                <div class="header-navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <button  class="navbar-toggle collapsed" data-toggle="collapse" data-target=".site-navigation" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>                        
                            <div class="logo navbar-brand">
                                <div class="logo-image">
                                    <a href="<?= $baseUrl ?>"><img src="image/logo-1.png" class="image-logo" alt="logo" /></a>
                                </div>
                            </div><!-- end logo -->
                            <nav id="primary-navigation" class="site-navigation navbar-collapse collapse" role="navigation">
                                <div class="nav-menu">
                                    <ul id="menu-all-pages" class="menu">
                                        <li class="active"><a href="<?= $baseUrl ?>">HOME</a></li>
                                        <li><a href="<?= $baseUrl ?>products.php">PRODUCTS</a></li>
                                        <li><a href="<?= $baseUrl ?>gallery.php">FACTORY GALLERY</a></li>
                                        <li><a href="<?= $baseUrl ?>about.php">ABOUT</a></li>
                                        <li><a href="<?= $baseUrl ?>contact.php">CONTACT</a></li>
                                        <li><a href="<?= $baseUrl ?>#appointment">Make an Appoinment</a></li>
                                    </ul>
                                </div><!-- end nav-menu -->
                            </nav><!-- end #primary-navigation -->
                            <div class="appoinment-header">
                                <a href="<?= $baseUrl ?>login/login.php"  target="_blank" class="btn btn-md btn-default" style="width:auto; background-color: #103b7a; color: #f9c937">Login</a>
                            </div>
                        </div><!-- end row -->
                    </div><!-- end container-fluid -->
                </div><!-- end header-navigation -->
            </header><!-- end #masthead -->            
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php
                    $result1 = $mysqli->query("SELECT * FROM slider ORDER BY sliderId");
                    $res = $result1->fetch_object();
                    ?>  
                    <div class="item active">
                        <img src="<?= $baseUrl ?>admin/upload/slider/<?php echo $res->pimg ?>" alt="Chicago" style="width:100%;height:500px;">
                    </div>  
                    <?php
                    if ($c > 0) {
                        $resul = $mysqli->query("SELECT * FROM slider ORDER BY sliderId DESC LIMIT $c");
                        while ($res = $resul->fetch_object()) {
                            ?>                    
                            <div class="item ">
                                <img src="<?= $baseUrl ?>admin/upload/slider/<?php echo $res->pimg ?>" alt="Chicago" style="width:100%;height:500px;">
                            </div>   
                            <?php
                        }
                    }
                    ?>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <br><br>      
            <article  id="page-1684" class="page container post-1684 type-page status-publish hentry">
                <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                    <div class="so-widget-title-widget so-widget-title-widget-base">
                        <div class="the-title text-center dark">
                            <h3>OUR SERVICE</h3><br>
                        </div>
                        <br>
                        <div class="so-widget-service-widget so-widget-service-widget-base">
                            <?php
                            $i = 0;
                            $result = $mysqli->query("SELECT * FROM service where serviceStutus='Active'");
                            while ($res = $result->fetch_object()) {
                                if ($i == 0 || $i % 2 == 0) {
                                    echo '<div class="row">';
                                }
                                $i++;
                                echo '<div class="col-md-6">';
                                echo '<div class="service-col" style="width: 100%">';
                                echo '<h3>';
                                echo $res->serviceName;
                                echo '</h3>';
                                echo '<div class="service-content">';
                                echo $res->serviceDis;
                                ?>
                                <br>
                                <a data-toggle="modal" href="#myModal<?php echo $res->serviceId; ?>" style="color: #0099cc;">View Details</a>
                                <!-- Modal -->
                                <div class="modal fade" id="myModal<?php echo $res->serviceId; ?>" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" style="align:right;">&times;</button>
                                                <h3 class="modal-title">
                                                    <?php echo $res->serviceName; ?>
                                                </h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <?php echo $res->serviceDis; ?>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" style=" background-color: #103b7a; color: #f9c937 ">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                echo '</div>';
                                echo '<figure>';
                                ?>
                                <img src = "<?= $baseUrl ?>admin/upload/service/<?php echo $res->pimg ?>" alt = "<?php echo $res->serviceName ?>" class="rounded-circle" style="width:80px; height:80px;">
                                <?php
                                echo '</figure>';
                                echo '</div>';
                                echo '</div>';
                                if ($i % 2 == 0) {
                                    echo '</div>';
                                } else {
                                    continue;
                                }
                            }
                            ?>  
                        </div>
                    </div>
                </div>
                <br>
                <div  style="padding: 80px;background-image: url(image/05/aabg_2.jpg);background-size: cover;" data-stretch-type="full" >                              
                    <div class="white-box-new panel-widget-style" style="padding: 35px 0px 40px 40px;opacity: 0.9;background-color:#ffffff;color: #ffffff;" >                                                              
                        <div class="the-title text-center dark">
                            <h3>About Amin Associates</h3>
                        </div>
                        <div class="about-content">
                            <p>
                                <?php
                                $result = $mysqli->query("SELECT * FROM about");
                                while ($res = $result->fetch_object()) {
                                    echo html_entity_decode($res->about);
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                    <div class="so-widget-title-widget so-widget-title-widget-base">
                        <div class="the-title text-center dark">
                            <h3>MANAGEMENT TEAM</h3>
                        </div>
                        <div class="row">
                            <?php
                            $result = $mysqli->query("SELECT * FROM member");
                            while ($res = $result->fetch_object()) {
                                ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="team-member">
                                        <div class="team-img">
                                            <img src="<?= $baseUrl ?>admin/upload/<?php echo $res->pimg ?>" alt="<?php echo $res->memName ?>" class="img-responsive" style="width:360px;height:310px;">
                                        </div>
                                        <div class="team-hover">
                                            <div class="desk">
                                                <h4>Hi There !</h4>
                                                <p><?php echo $res->memDis ?></p>
                                            </div>
                                            <div class="s-link">
                                                <a href="<?php echo $res->memFacebook ?>"><i class="fa fa-facebook"></i></a>
                                                <a href="<?php echo $res->memTwiter ?>"><i class="fa fa-instagram"></i></a>
                                                <a href="<?php echo $res->memGoogle ?>"><i class="fa fa-google-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="team-title">
                                        <h5><?php echo $res->memName ?></h5>
                                        <span><?php echo $res->memRole ?></span>
                                    </div>
                                </div>                            
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                    <div class="so-widget-title-widget so-widget-title-widget-base">
                        <div class="the-title text-center dark">
                            <h3>BRAND PARTNER</h3>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        $result = $mysqli->query("SELECT * FROM brand");
                        while ($res = $result->fetch_object()) {
                            ?>
                            <div class="col-md-3 col-sm-4">                                    
                                <br>
                                <img src = "<?= $baseUrl ?>admin/upload/brand/<?php echo $res->pimg ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $res->brandName ?>" class="img-thumbnail" alt="<?php echo $res->brandName ?>" style="width :185px; height : 70px">
                            </div>
                        <?php }
                        ?>
                    </div>
                </div><!-- end inner -->
                <br><br>
                <div class="panel-grid-cell" id="pgc-1684-4-0" >
                    <div class="so-widget-faq-widget so-widget-faq-widget-base">
                        <div class="faq-contact-section">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading-1682">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1682" aria-expanded="true" aria-controls="collapse-1682">
                                            <h4  class="panel-title">How do we manage our marketing policies  ?	</h4>
                                        </a>
                                    </div>
                                    <div id="collapse-1682"  role="tabpanel" aria-labelledby="heading-1682">
                                        <div class="panel-body">
                                            <p >We have experienced teams for marketing, who visit the factories per their requirements and visit customer?s office and collect samples. They send these samples to our factories to develop sample as per customer?s requirement. These samples are sent to the customers for their quality satisfaction and finally approval of the orders. This service is provided at 24 to 48 hours delivery of the goods and maintain customer relation by using mobile phone, fax, e-mail, and when necessary the converse directly.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end .panel collapse -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading-1681">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1681" aria-expanded="true" aria-controls="collapse-1681">
                                            <h4 class="panel-title">What kind of design pattern we follow ?					</h4>
                                        </a>
                                    </div>
                                    <div id="collapse-1681" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1681">
                                        <div class="panel-body">
                                            <p>We create designs according to our client?s specification that they provide us sketch or idea or sometimes physical samples. We research on basis and create the very best, imitate the original.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end .panel collapse -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading-1680">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1680" aria-expanded="true" aria-controls="collapse-1680">
                                            <h4 class="panel-title">How we deliver product sample to our client ?</h4>                                        
                                        </a>
                                    </div>
                                    <div id="collapse-1680" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1680">
                                        <div class="panel-body">
                                            <p>It is the key point of our starting business relationship. We take 3-7 days for any complete sample development. We provide our buyer?s free of cost samples and confirm right quality &amp; quantity at right time. We ensure that samples are made according to the client?s specification, as color, as shade etc. We also recreate and modify if needed for buyer?s styling &amp; cost purpose.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end .panel collapse -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading-1679">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1679" aria-expanded="true" aria-controls="collapse-1679">
                                            <h4 class="panel-title"> How we manages our quality control factors ?</h4>
                                        </a>
                                    </div>
                                    <div id="collapse-1679" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1679">
                                        <div class="panel-body">
                                            <p>Quality control covers all the work of controlling efficiency for the purpose of guarantying the required quality of the buyers and in order to optimize the cost involvement of all the process needed to manufacture the product. It covers the stages starting with procurement of quality yarn, dyes, chemicals, and laboratory testing for color, shades, color fastness, process control, inspection and packing and ending with delivery.</p>
                                            <p>We have well trained, skilled &amp; experienced teams who make our products different and helps to keep our commitment. By machinery process and our personal effort we set up for sample testing to prepare our products for manufacturing &amp; by turns to the final delivery.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end .panel collapse -->
                                </div><!-- end panel -->
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading-1643">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1643" aria-expanded="true" aria-controls="collapse-1643">
                                            <h4 class="panel-title">How we measure our row material quality ?</h4>
                                        </a>
                                    </div>
                                    <div id="collapse-1643" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-1643">
                                        <div class="panel-body">
                                            <p>We have strong source for raw materials both in local &amp; foreign. We always ensure quality &amp; standard raw materials and select the apt raw from our wide range of collection for starting the sampling process &amp; later the same for bulk after approval.</p>
                                        </div><!-- end panel-body -->
                                    </div><!-- end .panel collapse -->
                                </div><!-- end panel -->
                            </div><!-- end panel-group -->
                        </div>
                    </div>
                </div>
                <div id="appointment" class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">
                    <div class="the-title text-center dark">
                        <h3>CONTACT</h3>
                    </div>
                    <br>
                    <div class="container">
                        <div class="row col-md-12">
                            <?php
                            $result = $mysqli->query("SELECT * FROM contact");
                            while ($res = $result->fetch_object()) {
                                ?>
                                <div class="col-sm-6">
                                    <p><strong><span class="glyphicon glyphicon-user"></span>
                                            <?php
                                            echo $res->contactName;
                                            ?></strong><br />
                                        <?php
                                        echo $res->contactRole;
                                        ?><br />
                                        <span class="glyphicon glyphicon-phone"></span><?php echo $res->contactPhone; ?>

                                    </p>
                                </div>
                                <?php
                            }
                            ?>
                        </div><!-- end content-box -->
                    </div>        
                    <form action="msg.php" method="post">
                        <p class="team-title"><em>If you have any query about us, please make yourself comfortable and feel free to contact with us.</em></p>
                        <?php
                        if (isset($_GET['msg'])) {
                            ?>
                            <div class = "alert alert-success">
                                <strong>SUCCESS!</strong> Your message has been send!!!
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <p><input type="text" name="msgName" size="40"  placeholder="Your name" required="required"/></p>
                            </div>
                            <div class="col-md-6">
                                <p><input type="email" name="msgEmail" size="40" placeholder="Your email example@gmail.com" required="required"/></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p><input type="text" name="msgPhn" minlength="11" maxlength="14" placeholder="Your phone number 01XXXXXXXXX" pattern="^(?:\+?88|0088)?01[15-9]\d{8}$" required="required"/></p>
                                <span class="validity"></span>
                            </div>
                            <div class="col-md-6">
                                <p><input type="text" name="msgSub" size="40" placeholder="Subject Here" required="required"/></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p><textarea name="msgMsg" cols="30" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" style="resize: vertical;" placeholder="Type your message here.." required="required"></textarea></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p><input type="submit" name="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit btn btn-lg btn-outline" style="width:auto; background-color: #ffffff; color: #f9c937;"/></p>
                            </div>
                        </div>
                    </form>
                </div>
            </article>
            <br>
            <div class="siteorigin-panels-stretch panel-row-style" style="padding: 30px;background-color:#f4f4f4;" data-stretch-type="full">                    
                <div class="so-widget-title-widget so-widget-title-widget-base">
                    <div class="the-title text-center dark">
                        <h3>LOCATION</h3>
                    </div>
                    <div class="content-box">
                        <?php
                        $result = $mysqli->query("SELECT * FROM address");
                        while ($res = $result->fetch_object()) {
                            ?>
                            <p><?php echo $res->location ?></p>
                        <?php }
                        ?>
                    </div>
                    <!-- Add Google Maps -->
                    <?php
                    $result = $mysqli->query("SELECT * FROM address");
                    $res = $result->fetch_object();
                    ?>
                    <iframe src="https://www.google.com/maps/embed?pb=<?php echo $res->map ?>" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <!--footer start -->
        <div class="footer-credit">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copy">Amin Associates  2016 - Powered by <a href="http://www.olivineltd.com/"> OLIVINE LIMITED</a></p>
                    </div><!-- end column -->
                    <div class="col-md-6">
                        <ul class="list-socmed">
                            <li class="twitter soc-icon"><a href="http://twitter.com/#" class="fa fa-twitter"></a></li>
                            <li class="google soc-icon"><a href="http://google.com/#" class="fa fa-google-plus"></a></li>
                            <li class="facebook soc-icon"><a href="http://facebook.com/#" class="fa fa-facebook"></a></li>
                            <li class="linkedin soc-icon"><a href="http://linkedin.com/#" class="fa fa-linkedin"></a></li>
                            <li class="pinterest soc-icon"><a href="http://pinterest.com/#" class="fa fa-pinterest"></a></li>
                            <li class="youtube soc-icon"><a href="https://www.youtube.com/" class="fa fa-youtube"></a></li>
                            <li class="instagram soc-icon"><a href="https://instagram.com/" class="fa fa-instagram"></a></li>
                        </ul>
                    </div><!-- end column -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end footer-credit -->
        <!-- #main wrapper end-->
        <script>
            $(document).ready(function () {
                // Add smooth scrolling to all links in navbar + footer link
                $(".navbar a, footer a[href='#myPage']").on('click', function (event) {
                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 900, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    } // End if
                });

                $(window).scroll(function () {
                    $(".slideanim").each(function () {
                        var pos = $(this).offset().top;

                        var winTop = $(window).scrollTop();
                        if (pos < winTop + 600) {
                            $(this).addClass("slide");
                        }
                    });
                });
            })
        </script>
        <script>
            $(document).ready(function () {
                $("#myBtn").click(function () {
                    $("#myModal").modal();
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>