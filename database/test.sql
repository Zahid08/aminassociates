-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2018 at 03:17 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `aboutId` int(10) NOT NULL DEFAULT '1',
  `about` text NOT NULL,
  `pimg` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`aboutId`, `about`, `pimg`) VALUES
(1, 'The Amin Associates Ltd. is a symbol of efficiency, reliability, experience and innovative ideas. We produce high quality Belts, Elastics, Twill Tapes, Hang Tags, Photo Inlayers, Woven Labels, Printed Labels, Metal Buckles/ D-rings, Sewing Thread, Jacquard Elastics, Jacquard Tapes, Cotton Laces, Bra & Panty Laces, Gum Tape, Drawstrings & Cord etc. Our products are trust worthy, hence we always maintain customers testing requirements before the bulk production. We always emphasize on the comfort of our customers, as we are well aware most of our products are used in Kids & Children?s garments. \r\n<br><br>\r\nWe plan our production according to the market demand and can produce any quality provided by the buyer with the sufficient machine capacity. We maintain strict pre-caution to maintaining quality and produce them with zero defect efficiency. The raw materials we use are of A- grade & Oekotex certified imported from abroad (China, Taiwan, Thailand &Malaysia) and therefore excellent in quality. About color we can ensure you 100% yarn dyed without harmful reagents and objects.                                                                ', '5ad58ff5dcc18amin_associates_factory.png');

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `addressId` int(5) NOT NULL DEFAULT '1',
  `location` text,
  `phone` text,
  `email` text,
  `openHour` text,
  `map` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`addressId`, `location`, `phone`, `email`, `openHour`, `map`) VALUES
(1, '243, ChowaryrTak, Faidabad, Dakhian Khan, Abdullahpur, Uttara, Dhaka-1230. Bangladesh.', '+8801715416197, +8801929997975-99, +88-02-8957065, +88-02-8951290, +88-02-8957052, +88-02-8957053', 'aminassociates@yahoo.com\r\nsales@aminassociates.com\r\ninfo@aminassociates.com\r\naminassociatesbd@gmail.com', 'Monday - Friday 08:00 AM - 04:00 PM ', '!1m18!1m12!1m3!1d3648.284381402239!2d90.4070647149838!3d23.879532484525814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c435ba46b32b%3A0xe570b29093b53ac1!2sAmin+Associates+Limited!5e0!3m2!1sen!2sbd!4v1523967527502');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminId` int(20) NOT NULL,
  `adminName` varchar(50) NOT NULL,
  `adminPass` varchar(50) NOT NULL,
  `adminPhn` varchar(20) NOT NULL,
  `adminEmail` varchar(50) NOT NULL,
  `adminPic` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminId`, `adminName`, `adminPass`, `adminPhn`, `adminEmail`, `adminPic`) VALUES
(1, 'Admin', '8df32286deed62bae5bf746672805c78', '01785401474', 'superadmin@gmail.com', '5ad5f15bc0bafimages (3).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brandId` int(20) NOT NULL,
  `brandName` varchar(200) NOT NULL,
  `brandUrl` varchar(200) NOT NULL,
  `pimg` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brandId`, `brandName`, `brandUrl`, `pimg`) VALUES
(1, 'zara', 'zara', '5ad5912417742Zara.png'),
(2, 'AEROPOSTALE', 'AEROPOSTALE', '5ad5914aae26d01.png'),
(3, 'AMERICAN EAGLE', 'AMERICAN EAGLE', '5ad5916003a241200px-American_Eagle.png'),
(4, 'ASDA', 'ASDA', '5ad5916eecaf8Asda.png'),
(5, 'C-A', 'C-A', '5ad59183c6dceC-A.png'),
(6, 'CELIO', 'CELIO', '5ad5919430cfcCelio.png'),
(7, 'Dickies', 'Dickies', '5ad591a1647bdDickies.png'),
(8, 'GAP', 'GAP', '5ad591b58b266GPA.png'),
(9, 'GYMBOREE', 'GYMBOREE', '5ad591c430851Gymboree.png'),
(10, 'H&M', 'H&M', '5ad591dd13132HM.png'),
(11, 'jcpenney', 'jcpenney', '5ad591fb4458bJcepenney.png'),
(12, 'KappAh', 'KappAh', '5ad592097a859KappahL.png'),
(13, 'K mart', 'K mart', '5ad59215e31e4K-Mart.png'),
(14, 'MS', 'MS', '5ad5922602912MS.png'),
(15, 'next', 'next', '5ad59232e01f9Next.png'),
(16, 'NIKE', 'NIKE', '5ad5924907ef2Nike.png'),
(17, 'Sears', 'Sears', '5ad59255d5d85Sears.png'),
(18, 'Target', 'Target', '5ad59261904c0Target.png'),
(19, 'TESCO', 'TESCO', '5ad5926dba3ffTesco.png'),
(20, 'WAL MART', 'WAL MART', '5ad59279f1926Walmart.png');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contactId` int(50) NOT NULL,
  `contactName` varchar(200) NOT NULL,
  `contactPhone` text NOT NULL,
  `contactRole` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contactId`, `contactName`, `contactPhone`, `contactRole`) VALUES
(1, 'Md. Khaled Amin Rashed', '01929-997981', 'For Any Enquiry'),
(2, 'Md. Habibur Rahman', '01929-997986', 'Marketing'),
(3, 'Md. Mijanur Rahman', '01929-997990', 'Delivery & Quality'),
(4, ' Mumtahina Zaman Bintu', '01929-997980', 'Sr. Business Development Manager');

-- --------------------------------------------------------

--
-- Table structure for table `factory`
--

CREATE TABLE `factory` (
  `facId` int(50) NOT NULL,
  `facName` varchar(100) NOT NULL,
  `pimg` varchar(200) NOT NULL,
  `facDis` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `factory`
--

INSERT INTO `factory` (`facId`, `facName`, `pimg`, `facDis`) VALUES
(1, '', '5ad594b4ebfb201.jpg', ''),
(2, '', '5ad594bb865771.jpg', ''),
(3, '', '5ad594c1a2dfe02.jpg', ''),
(4, '', '5ad594c842ac903.jpg', ''),
(5, '', '5ad594ce93fd93.jpg', ''),
(6, '', '5ad594d4c0fdb05.jpg', ''),
(7, '', '5ad594db0b36a5.jpg', ''),
(8, '', '5ad594e18668406.jpg', ''),
(9, '', '5ad594e849bf207.jpg', ''),
(10, '', '5ad594ee9fc4508.jpg', ''),
(11, '', '5ad594f4d6d1910.jpg', ''),
(12, '', '5ad594fb436b412.jpg', ''),
(13, '', '5ad59502d813613.jpg', ''),
(14, '', '5ad595090fdbb15.jpg', ''),
(15, '', '5ad595107be9816.jpg', ''),
(16, '', '5ad59515b791119.jpg', ''),
(17, '', '5ad5951c52c9420.jpg', ''),
(18, '', '5ad59522c196621.jpg', ''),
(19, '', '5ad595297ba0522.jpg', ''),
(20, '', '5ad595309b2a523.jpg', ''),
(21, '', '5ad595355345d24.jpg', ''),
(22, '', '5ad5953b741fd25.jpg', ''),
(23, '', '5ad595419aee626.jpg', ''),
(24, '', '5ad59547dae6931.jpg', ''),
(25, '', '5ad5954d3396aamin_associates_factory.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `memId` int(50) NOT NULL,
  `memName` varchar(50) NOT NULL,
  `pimg` varchar(200) NOT NULL,
  `memDis` varchar(2000) NOT NULL,
  `memGoogle` varchar(100) NOT NULL,
  `memTwiter` varchar(100) NOT NULL,
  `memFacebook` varchar(100) NOT NULL,
  `memRole` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`memId`, `memName`, `pimg`, `memDis`, `memGoogle`, `memTwiter`, `memFacebook`, `memRole`) VALUES
(1, 'ALAMGIR HOSSAIN HANNAN', '5ad5c1630c8b8user.png', 'ALAMGIR HOSSAIN HANNAN', 'https://plus.google.com/discover', 'https://www.instagram.com/', 'https://www.facebook.com/', 'FOUNDER & CEO'),
(2, 'KHALED AMIN RASHED', '5ad5c5b73e515user.png', 'KHALED AMIN RASHED', 'https://plus.google.com/discover', 'https://www.instagram.com/', 'https://www.facebook.com/', 'HR MANAGER'),
(3, 'MIZANUR RAHMAN', '5ad5c617efd8cuser.png', 'MIZANUR RAHMAN', '', '', '', 'MARKETING MANAGER'),
(4, 'HABIBUR RAHAMAN', '5ad5c659449c3user.png', 'HABIBUR RAHAMAN', '', '', '', 'FOUNDER & CEO'),
(5, 'MOHD. SHAHAJAN', '5ad5c6733253euser.png', 'MOHD. SHAHAJAN', '', '', '', 'FOUNDER & CEO'),
(6, 'ISMAIL HOSSAIN NOHEL', '5ad5c686b36b9user.png', 'ISMAIL HOSSAIN NOHEL', '', '', '', 'FOUNDER & CEO'),
(7, 'MUMTAHINA ZAMAN BINTU', '5ad5c69fe831auser.png', 'MUMTAHINA ZAMAN BINTU', '', '', '', 'FOUNDER & CEO'),
(8, 'ANOWAR HOSSAIN', '5ad5c6af8e203user.png', 'ANOWAR HOSSAIN', '', '', '', 'FOUNDER & CEO');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `msgId` int(50) NOT NULL,
  `msgName` varchar(50) NOT NULL,
  `msgEmail` varchar(50) NOT NULL,
  `msgPhn` varchar(20) NOT NULL,
  `msgSub` varchar(200) NOT NULL,
  `msgMsg` text NOT NULL,
  `stutus` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pid` int(11) NOT NULL,
  `ptype` varchar(100) NOT NULL,
  `pname` varchar(50) NOT NULL,
  `pimg` varchar(100) NOT NULL,
  `pdiscrip` varchar(2000) NOT NULL,
  `pstutus` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pid`, `ptype`, `pname`, `pimg`, `pdiscrip`, `pstutus`) VALUES
(1, 'Belt', 'Lather', '5ad5974d1dc2a8.jpg', '', 'Active'),
(2, 'Belt', 'Off White', '5ad597e71d12fbelt-02-188x188.jpg', '', 'Active'),
(3, 'Belt', 'Ash Gray', '5ad59805ec11fbelt-06-188x188.jpg', '', 'Active'),
(4, 'Belt', 'Elastic', '5ad5981970d6ebelt-10-188x188.jpg', '', 'Active'),
(5, 'Belt', 'Blue', '5ad5982de9975belt-11-188x188.jpg', '', 'Active'),
(6, 'Belt', 'Red', '5ad59845544e8belt-12-188x188.jpg', '', 'Active'),
(7, 'Belt', 'Cotton', '5ad59859d2c8fCotton-Belt.jpg', '', 'Active'),
(8, 'Belt', 'Black', '5ad599a444842Elastic-Belt.jpg', '', 'Active'),
(9, 'Belt', 'Eyelet', '5ad599b8288a1Eyelet-Belt.jpg', '', 'Active'),
(10, 'Belt', 'Fancy', '5ad599c4a01d9Fancy-Belt-.jpg', '', 'Active'),
(11, 'Belt', 'Kids', '5ad599d6b9e1aKids-Belt.jpg', '', 'Active'),
(12, 'Belt', 'Poly', '5ad599e27fc45Poly-Belt.jpg', '', 'Active'),
(13, 'Belt', 'Pu', '5ad599ec1019bPU-belt.jpg', '', 'Active'),
(14, 'Belt', 'Special Designed', '5ad599fee507aSpecial-Designed-Belt.jpg', '', 'Active'),
(15, 'Drawstring Cord', 'Black', '5ad59b7a880b62-3.jpg', '', 'Active'),
(16, 'Drawstring Cord', 'White', '5ad59b8b7eca13-3.jpg', '', 'Active'),
(17, 'Drawstring Cord', 'Paste', '5ad59b9c0ae824-2.jpg', '', 'Active'),
(18, 'Drawstring Cord', 'Special Designed', '5ad59bf688b12Drawstring-With-DTM-Plastic-Tips.jpg', '', 'Active'),
(19, 'Elastic', 'Check ', '5ad59c752be5bCheck-Elastic.jpg', '', 'Active'),
(20, 'Elastic', 'Twill Tape', '5ad59cb1d399eTwill-Tape-.jpg', '', 'Active'),
(21, 'Elastic', 'Striped', '5ad59d09c70ce3-1.jpg', '', 'Active'),
(22, 'Jacquard', 'Black', '5ad59de29d1c71-1.jpg', '', 'Active'),
(23, 'Narrow Fabrics', 'Navy Blue', '5ad59e125def61n.jpg', '', 'Active'),
(24, 'Jacquard', 'Striped', '5ad59e2e4e4ed2-1.jpg', '', 'Active'),
(25, 'Narrow Fabrics', 'Red - White', '5ad5a5ed980ec2jjhggg.jpg', '', 'Active'),
(26, 'Jacquard', 'Special designed', '5ad59e57f14994.jpg', '', 'Active'),
(27, 'Jacquard', 'Special Designed 2', '5ad59e781ce8d5.jpg', '', 'Active'),
(28, 'Narrow Fabrics', 'Black - White', '5ad59ea475214DSC08785.jpg', '', 'Active'),
(29, 'Narrow Fabrics', 'Corner Red', '5ad59ee4a2363ffff.jpg', '', 'Active'),
(30, 'Narrow Fabrics', 'Red Striped', '5ad59efc690e1hgg.jpg', '', 'Active'),
(31, 'Narrow Fabrics', 'Red - Ash', '5ad59f101a3b6jku.jpg', '', 'Active'),
(32, 'Narrow Fabrics', 'Off White-Black', '5ad59f320a73dmm.jpg', '', 'Active'),
(33, 'Narrow Fabrics', 'Striped Green', '5ad59f494295ftape.jpg', '', 'Active'),
(34, 'Labels', 'Michael', '5ad59fee689d01-4.jpg', '', 'Active'),
(35, 'Labels', 'Funky Diva', '5ad5a008a7e5c3-4.jpg', '', 'Active'),
(36, 'Labels', 'Reject', '5ad5a01ddcbb04-3.jpg', '', 'Active'),
(37, 'Labels', 'Custom Designed', '5ad5a0484b52c5-2.jpg', '', 'Active'),
(38, 'Lace', 'Pink', '5ad5a053012b61.BPLess.jpg', '', 'Active'),
(39, 'Lace', 'Purple', '5ad5a07e99fa83-5.jpg', '', 'Active'),
(40, 'Lace', 'Floral', '5ad5a0bbf18cc4-4.jpg', '', 'Active'),
(41, 'Lace', 'Custom Designed', '5ad5a0f76e7c32-5.jpg', '', 'Active'),
(42, 'Metal D-Ring Buckle', 'Kye Ring', '5ad5a114ca28b3.jpg', '', 'Active'),
(43, 'Metal D-Ring Buckle', 'D-Ring', '5ad5a12d6fe96dring-01.jpg', '', 'Active'),
(44, 'Metal D-Ring Buckle', 'Front Page Brass', '5ad5a152f10deFront-Page_Brass.jpg', '', 'Active'),
(45, 'Metal D-Ring Buckle', 'Sell metal D-Ring', '5ad5a1823eed1Sell_metal_D_ring_O.jpg', '', 'Active'),
(46, 'Paper Items', 'Paper Print', '5ad5a1d45a0552-4.jpg', '', 'Active'),
(47, 'Rubber Thread', 'Item 1', '5ad5a2722ca6f1-2.jpg', '', 'Active'),
(48, 'Rubber Thread', 'Item 2', '5ad5a28f3071a2.jpg', '', 'Active'),
(49, 'Rubber Thread', 'Item 3', '5ad5a29dbf4d43-2.jpg', '', 'Active'),
(50, 'Rubber Thread', 'Item 4', '5ad5a2aea19ab4-1.jpg', '', 'Active'),
(51, 'Rubber Thread', 'Item 5', '5ad5a2c4287e65-1.jpg', '', 'Active'),
(52, 'Sewing Thread', 'Item 1', '5ad5a2f3484f3swing-01.jpg', '', 'Active'),
(53, 'Sewing Thread', 'Item 2', '5ad5a302a961bswing-02.jpg', '', 'Active'),
(54, 'Sewing Thread', 'Item 3', '5ad5a314846b4swing-03.jpg', '', 'Active'),
(55, 'Sewing Thread', 'Item 4', '5ad5a3284b939Thread.jpg', '', 'Active'),
(56, 'Yarn Dyeing', 'Item 1', '5ad5a333b088c1.jpg', '', 'Active'),
(57, 'Yarn Dyeing', 'Item 2', '5ad5a5af1c9c7Yarn_Dyein.jpg', '', 'Active'),
(58, 'Lace', 'custom 2', '5ad5a60e5cfa15-3.jpg', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `productname`
--

CREATE TABLE `productname` (
  `proid` int(20) NOT NULL,
  `proname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productname`
--

INSERT INTO `productname` (`proid`, `proname`) VALUES
(1, 'Belt'),
(2, 'Drawstring Cord'),
(3, 'Elastic'),
(4, 'Jacquard'),
(5, 'Labels'),
(6, 'Lace'),
(7, 'Metal D-Ring Buckle'),
(8, 'Narrow Fabrics'),
(9, 'Paper Items'),
(10, 'Rubber Thread'),
(11, 'Sewing Thread'),
(12, 'Yarn Dyeing');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `serviceId` int(50) NOT NULL,
  `serviceName` varchar(200) NOT NULL,
  `pimg` varchar(200) NOT NULL,
  `serviceDis` varchar(2000) NOT NULL,
  `serviceStutus` enum('Active','Inactive') NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`serviceId`, `serviceName`, `pimg`, `serviceDis`, `serviceStutus`) VALUES
(1, 'Amin Belt Industries Ltd.', '5ad5a7b0b8f9dElastic-Belt-80x80.jpg', '100% Export Oriented Complete Woven Belt Manufacturing Company.', 'Active'),
(2, 'Amin Narrow Fabrics Ltd.', '5ad5a7cf031b02-80x80.jpg', 'All Kinds of Elastic, Non Elastic Band, String & Cord Manufacturer, Exporter & Supplier.', 'Active'),
(3, 'Amin Lace Industries Ltd.', '5ad5a7ed84d1a5-3-80x80.jpg', 'Cotton Lace, Braided Lace, Crochet Lace, Bra& Panty Lace Manufacturer, Exporter, Importer & Supplier.', 'Active'),
(4, 'S.M. Enterprise.', '5ad5af9c73b224-3-80x80.jpg', 'All Kinds of Paper Items, Label Manufacturer & Supplier.', 'Active'),
(5, 'Amin Elastic (Computerized) Industries Ltd.', '5ad5a850160763-1-80x80.jpg', 'Export Oriented Jacquard Elastic, Woven Elastic & Check Elastic Manufacturing Unit.', 'Active'),
(6, 'Amin Thread Ltd.', '5ad5af15cd3a6Thread-80x80.jpg', '100% Export Oriented Spun Polyester, Cotton, Filament Polyester Sewing Thread Manufacturer & Dyeing Unit.', 'Active'),
(7, 'Jahan Printing & Accessories Ltd.', '5ad5af405fe0dScreen_Print-80x80.jpg', 'A House of Quality Screen Print.', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `sliderId` int(50) NOT NULL,
  `sliderName` varchar(50) NOT NULL,
  `pimg` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`sliderId`, `sliderName`, `pimg`) VALUES
(1, '1', '5ad5c71ca8a2f4-1.jpg'),
(2, '2', '5ad5c72d3f36612.jpg'),
(3, '3', '5ad5cbef63a0f130731-133204.jpg'),
(4, '4', '5ad5cc1479784category_bg_women_belt.jpg'),
(5, '6', '5ad5cacfd1dbd02e31f4c0f5b7cfbaf5f39338a723792.jpg'),
(6, '7', '5ad5c983e19fbHTB1pJPNSpXXXXaTaXXXq6xXFXXXN.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`aboutId`);

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`addressId`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brandId`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contactId`);

--
-- Indexes for table `factory`
--
ALTER TABLE `factory`
  ADD PRIMARY KEY (`facId`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`memId`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`msgId`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `productname`
--
ALTER TABLE `productname`
  ADD PRIMARY KEY (`proid`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`serviceId`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`sliderId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brandId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contactId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `factory`
--
ALTER TABLE `factory`
  MODIFY `facId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `memId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `msgId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `productname`
--
ALTER TABLE `productname`
  MODIFY `proid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `serviceId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `sliderId` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
